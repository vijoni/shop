<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedPayment;

class Payment extends GeneratedPayment
{
  public const TYPE_ONETIME_PAYMENT = 'onetime_payment';
  public const TYPE_INSTALLMENTS = 'installments';

  public const PROVIDER_STRIPE_CREDIT_CARD = 'stripe_credit_card';
  public const PROVIDER_SEPA = 'sepa';
  public const PROVIDER_ZAB_INSTALLMENTS = 'zab_installments';

  public function __construct(
    string $type = '',
    string $provider = '',
    string $variant = ''
  ) {
    $this->setType($type);
    $this->setProvider($provider);
    $this->setVariant($variant);
  }
}
