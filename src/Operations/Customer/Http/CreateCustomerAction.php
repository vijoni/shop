<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Http;

use Vijoni\Application\Http\JsonResponse;
use Vijoni\Application\Result;
use Vijoni\Operations\Customer\ModuleFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Operations\Customer\UseCase\CreateCustomerUseCase;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class CreateCustomerAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $moduleFactory = $this->moduleFactory();

    $createCustomerRequest = $moduleFactory->newCreateCustomerRequest();
    $createCustomerRequest->use($request->getBody()->getContents());
    $errors = $createCustomerRequest->validate();

    if (!empty($errors)) {
      return new JsonResponse(400, [], own_json_encode($errors));
    }

    $customer = $createCustomerRequest->createCustomer();
    $result = $moduleFactory->newCreateCustomerUseCase()->createCustomer($customer);

    if ($this->isDuplicate($result)) {
      return new JsonResponse(409, [], '{}');
    }

    return new JsonResponse(201, [], '{}');
  }

  private function isDuplicate(Result $result): bool
  {
    if ($result->isSuccess()) {
      return false;
    }

    $errorCode = $result->getFirstError()->getErrorCode();

    return $errorCode === CreateCustomerUseCase::ERROR_DUPLICATE_ENTRY;
  }
}
