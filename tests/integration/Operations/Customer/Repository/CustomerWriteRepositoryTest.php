<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Operations\Customer\Repository;

use Codeception\Test\Unit;
use Vijoni\Operations\Customer\Repository\CustomerReadRepository;
use Vijoni\Operations\Customer\Repository\CustomerWriteRepository;
use Vijoni\Operations\Customer\Repository\MapperFactory;
use Vijoni\Operations\Shared\Customer;
use VijoniTest\Helper\Operations\CustomerBuilder;
use VijoniTest\IntegrationTester;

class CustomerWriteRepositoryTest extends Unit
{
  protected IntegrationTester $tester;

  public function testCreateCustomer(): void
  {
    $customer = CustomerBuilder::buildCustomer();

    $mapperFactory = new MapperFactory();
    $dbClient = $this->tester->shareDbClient();
    $writeRepository = new CustomerWriteRepository($dbClient, $mapperFactory);
    $readRepository = new CustomerReadRepository($dbClient, $mapperFactory);

    $dbClient->startTransaction();
    $writeRepository->createCustomer($customer);
    $readCustomer = $readRepository->findCustomerById($customer->getCustomerId());
    $dbClient->rollback();

    $createdCustomerProperties = $customer->toArray();
    $createdCustomerProperties[Customer::BIRTH_DATE] = $customer->getBirthDate()->format('Y-m-d H:i:s');
    $readCustomerProperties = $readCustomer->toArray();
    $readCustomerProperties[Customer::BIRTH_DATE] = $readCustomer->getBirthDate()->format('Y-m-d H:i:s');

    $this->assertSame($createdCustomerProperties, $readCustomerProperties);
  }

  public function testUpdateCustomer(): void
  {
    $newCustomer = CustomerBuilder::buildCustomer();

    $faker = $this->tester->faker();
    $updatedCustomer = new Customer();
    $updatedCustomer->setEmail($faker->email());

    $dbClient = $this->tester->shareDbClient();
    $mapperFactory = new MapperFactory();
    $writeRepository = new CustomerWriteRepository($dbClient, $mapperFactory);
    $readRepository = new CustomerReadRepository($dbClient, $mapperFactory);

    $dbClient->startTransaction();
    $writeRepository->createCustomer($newCustomer);
    $updatedCustomer->setCustomerId($newCustomer->getCustomerId());
    $writeRepository->updateCustomer($updatedCustomer);
    $readCustomer = $readRepository->findCustomerById($updatedCustomer->getCustomerId());
    $dbClient->rollback();

    $this->assertNotSame($newCustomer->getEmail(), $readCustomer->getEmail());
    $this->assertSame($updatedCustomer->getEmail(), $readCustomer->getEmail());
  }
}
