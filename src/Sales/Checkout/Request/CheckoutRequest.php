<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Request;

use Vijoni\Application\SchemaValidator\JsonSchemaValidator;
use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Sales\Shared\Region;
use Vijoni\Sales\Shared\ValidationMessagesMap;

class CheckoutRequest
{
  private const SCHEMA_FILEPATH = __DIR__ . '/schema-checkout.json';

  private string $payload = '';

  public function __construct(
    private JsonSchemaValidator $jsonSchemaValidator,
    private ValidationMessagesMap $messagesMap
  ) {
  }

  public function use(string $payload): void
  {
    $this->payload = $payload;
  }

  public function validate(): array
  {
    return $this->jsonSchemaValidator->validate(
      $this->payload,
      self::SCHEMA_FILEPATH,
      $this->messagesMap->readMap()
    );
  }

  /**
   * @return OrderItem[]
   */
  public function createOrderItems(): array
  {
    $payload = (array)json_decode($this->payload, true);
    $data = (array)$payload['data'];

    /** @var array[] $orderItems */
    $orderItems = (array)$data['orderItems'];

    return array_map($this->createOrderItem(...), $orderItems);
  }

  public function readRegion(): Region
  {
    $payload = (array)json_decode($this->payload, true);
    $data = (array)$payload['data'];

    /** @var string */
    $countryCode = $data['countryCode'];

    /** @var string */
    $locale = $data['locale'];

    return new Region($countryCode, $locale);
  }

  private function createOrderItem(array $orderItemAttributes): OrderItem
  {
    $orderItem = new OrderItem();
    $orderItem->fromArray([
      OrderItem::PRODUCT_ID => $orderItemAttributes['productId'],
      OrderItem::PRICE => (string)$orderItemAttributes['price'],
      OrderItem::COUNT => $orderItemAttributes['count'],
    ]);

    return $orderItem;
  }
}
