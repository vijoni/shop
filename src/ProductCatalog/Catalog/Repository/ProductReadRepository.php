<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog\Repository;

use Vijoni\ProductCatalog\Shared\ProductCollection;
use Vijoni\ProductCatalog\Shared\ProductDbTable;
use Vijoni\Database\Client\DatabaseClient;
use Vijoni\ProductCatalog\Shared\Region;

class ProductReadRepository
{
  public function __construct(private DatabaseClient $db, private MapperFactory $mapperFactory)
  {
  }

  /**
   * @param string[] $productIds
   * @return ProductCollection
   */
  public function findProductsByIdByRegion(array $productIds, Region $region): ProductCollection
  {
    $joinedProductIds = implode(',', $productIds);

    $query = <<<SQL
SELECT
  p.product_id,
  p.name,
  p.type,
  pl."global" || pl.%c || pl.%c as __attributes,
  string_agg(cp.product_id::text, ',') as __children_id,
  string_agg(c.count::text, ',') as __children_count
FROM product_catalog_product p
  JOIN product_catalog_product_localized pl ON p.dbid = pl.product_dbid
  LEFT JOIN product_catalog_product_children c ON p.dbid = c.parent_product_dbid
  LEFT JOIN product_catalog_product cp ON c.child_product_dbid = cp.dbid
WHERE p.product_id IN (%ls)
GROUP BY p.product_id, p.name, p.type, __attributes;
SQL;

    $countryCode = $region->getCountryCode();
    $locale = $region->getLocale();

    $commentTpl = 'product-catalog: find products by id by region; productIds:[%s] countryCode:[%s] locale:[%s]';
    $qb = $this->db->newQueryBuilder(
      $query,
      [$countryCode, $locale, $productIds],
      [
        '__attributes' => ProductDbTable::ALIAS_ATTRIBUTES,
        '__children_id' => ProductDbTable::ALIAS_CHILDREN_ID,
        '__children_count' => ProductDbTable::ALIAS_CHILDREN_COUNT,
      ],
      sprintf($commentTpl, $joinedProductIds, $countryCode, $locale)
    );

    $products = $this->db->query($qb);

    $productMapper = $this->mapperFactory->newProductMapper();

    return $productMapper->mapDbRowsToProducts($products);
  }
}
