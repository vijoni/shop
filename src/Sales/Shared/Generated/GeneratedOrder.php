<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedOrder extends ClassBase
{
  private string $orderId = '';
  private string $customerId = '';
  private string $purchaseId = '';
  private \Vijoni\Sales\Shared\Address $billingAddress;
  private \Vijoni\Sales\Shared\Address $shippingAddress;
  private string $priceToPay = '';
  private string $netPrice = '';
  private string $taxValue = '';
  private string $discount = '';
  private array $taxRateTotals = [];
  private string $voucherCode = '';
  private string $countryCode = '';
  private string $locale = '';
  private string $status = '';
  private array $orderItems = [];
  private \Vijoni\Sales\Shared\Payment $payment;
  private array $paymentDetails = [];
  private array $products = [];

  public const ORDER_ID = 'orderId';
  public const CUSTOMER_ID = 'customerId';
  public const PURCHASE_ID = 'purchaseId';
  public const BILLING_ADDRESS = 'billingAddress';
  public const SHIPPING_ADDRESS = 'shippingAddress';
  public const PRICE_TO_PAY = 'priceToPay';
  public const NET_PRICE = 'netPrice';
  public const TAX_VALUE = 'taxValue';
  public const DISCOUNT = 'discount';
  public const TAX_RATE_TOTALS = 'taxRateTotals';
  public const VOUCHER_CODE = 'voucherCode';
  public const COUNTRY_CODE = 'countryCode';
  public const LOCALE = 'locale';
  public const STATUS = 'status';
  public const ORDER_ITEMS = 'orderItems';
  public const PAYMENT = 'payment';
  public const PAYMENT_DETAILS = 'paymentDetails';
  public const PRODUCTS = 'products';

  public function __construct()
  {
    $this->setBillingAddress(new \Vijoni\Sales\Shared\Address());
    $this->setShippingAddress(new \Vijoni\Sales\Shared\Address());
    $this->setPayment(new \Vijoni\Sales\Shared\Payment());
  }

  public function getOrderId(): string
  {
    return $this->orderId;
  }

  public function setOrderId(string $orderId): void
  {
    $this->orderId = $orderId;
    $this->modified[self::ORDER_ID] = true;
  }

  public function getCustomerId(): string
  {
    return $this->customerId;
  }

  public function setCustomerId(string $customerId): void
  {
    $this->customerId = $customerId;
    $this->modified[self::CUSTOMER_ID] = true;
  }

  public function getPurchaseId(): string
  {
    return $this->purchaseId;
  }

  public function setPurchaseId(string $purchaseId): void
  {
    $this->purchaseId = $purchaseId;
    $this->modified[self::PURCHASE_ID] = true;
  }

  public function getBillingAddress(): \Vijoni\Sales\Shared\Address
  {
    return $this->billingAddress;
  }

  public function setBillingAddress(\Vijoni\Sales\Shared\Address $billingAddress): void
  {
    $this->billingAddress = $billingAddress;
    $this->modified[self::BILLING_ADDRESS] = true;
  }

  public function getShippingAddress(): \Vijoni\Sales\Shared\Address
  {
    return $this->shippingAddress;
  }

  public function setShippingAddress(\Vijoni\Sales\Shared\Address $shippingAddress): void
  {
    $this->shippingAddress = $shippingAddress;
    $this->modified[self::SHIPPING_ADDRESS] = true;
  }

  public function getPriceToPay(): string
  {
    return $this->priceToPay;
  }

  public function setPriceToPay(string $priceToPay): void
  {
    $this->priceToPay = $priceToPay;
    $this->modified[self::PRICE_TO_PAY] = true;
  }

  public function getNetPrice(): string
  {
    return $this->netPrice;
  }

  public function setNetPrice(string $netPrice): void
  {
    $this->netPrice = $netPrice;
    $this->modified[self::NET_PRICE] = true;
  }

  public function getTaxValue(): string
  {
    return $this->taxValue;
  }

  public function setTaxValue(string $taxValue): void
  {
    $this->taxValue = $taxValue;
    $this->modified[self::TAX_VALUE] = true;
  }

  public function getDiscount(): string
  {
    return $this->discount;
  }

  public function setDiscount(string $discount): void
  {
    $this->discount = $discount;
    $this->modified[self::DISCOUNT] = true;
  }

  /**
   * @return \Vijoni\Sales\Shared\Totals[]
   */
  public function getTaxRateTotals(): array
  {
    return $this->taxRateTotals;
  }

  public function setTaxRateTotals(array $taxRateTotals): void
  {
    $this->taxRateTotals = $taxRateTotals;
    $this->modified[self::TAX_RATE_TOTALS] = true;
  }

  public function getVoucherCode(): string
  {
    return $this->voucherCode;
  }

  public function setVoucherCode(string $voucherCode): void
  {
    $this->voucherCode = $voucherCode;
    $this->modified[self::VOUCHER_CODE] = true;
  }

  public function getCountryCode(): string
  {
    return $this->countryCode;
  }

  public function setCountryCode(string $countryCode): void
  {
    $this->countryCode = $countryCode;
    $this->modified[self::COUNTRY_CODE] = true;
  }

  public function getLocale(): string
  {
    return $this->locale;
  }

  public function setLocale(string $locale): void
  {
    $this->locale = $locale;
    $this->modified[self::LOCALE] = true;
  }

  public function getStatus(): string
  {
    return $this->status;
  }

  public function setStatus(string $status): void
  {
    $this->status = $status;
    $this->modified[self::STATUS] = true;
  }

  /**
   * @return \Vijoni\Sales\Shared\OrderItem[]
   */
  public function getOrderItems(): array
  {
    return $this->orderItems;
  }

  public function setOrderItems(array $orderItems): void
  {
    $this->orderItems = $orderItems;
    $this->modified[self::ORDER_ITEMS] = true;
  }

  public function getPayment(): \Vijoni\Sales\Shared\Payment
  {
    return $this->payment;
  }

  public function setPayment(\Vijoni\Sales\Shared\Payment $payment): void
  {
    $this->payment = $payment;
    $this->modified[self::PAYMENT] = true;
  }

  public function getPaymentDetails(): array
  {
    return $this->paymentDetails;
  }

  public function setPaymentDetails(array $paymentDetails): void
  {
    $this->paymentDetails = $paymentDetails;
    $this->modified[self::PAYMENT_DETAILS] = true;
  }

  /**
   * @return \Vijoni\Sales\Shared\Product[]
   */
  public function getProducts(): array
  {
    return $this->products;
  }

  public function setProducts(array $products): void
  {
    $this->products = $products;
    $this->modified[self::PRODUCTS] = true;
  }

  public function toArray(): array
  {
    return [
      self::ORDER_ID => $this->getOrderId(),
      self::CUSTOMER_ID => $this->getCustomerId(),
      self::PURCHASE_ID => $this->getPurchaseId(),
      self::BILLING_ADDRESS => $this->getBillingAddress(),
      self::SHIPPING_ADDRESS => $this->getShippingAddress(),
      self::PRICE_TO_PAY => $this->getPriceToPay(),
      self::NET_PRICE => $this->getNetPrice(),
      self::TAX_VALUE => $this->getTaxValue(),
      self::DISCOUNT => $this->getDiscount(),
      self::TAX_RATE_TOTALS => $this->getTaxRateTotals(),
      self::VOUCHER_CODE => $this->getVoucherCode(),
      self::COUNTRY_CODE => $this->getCountryCode(),
      self::LOCALE => $this->getLocale(),
      self::STATUS => $this->getStatus(),
      self::ORDER_ITEMS => $this->getOrderItems(),
      self::PAYMENT => $this->getPayment(),
      self::PAYMENT_DETAILS => $this->getPaymentDetails(),
      self::PRODUCTS => $this->getProducts(),
    ];
  }

  public function toDeepArray(): array
  {
    $asArray = $this->toArray();
    $asArray[self::BILLING_ADDRESS] = $this->getBillingAddress()->toDeepArray();
    $asArray[self::SHIPPING_ADDRESS] = $this->getShippingAddress()->toDeepArray();
    $asArray[self::TAX_RATE_TOTALS] = $this->collectionToArray($this->getTaxRateTotals());
    $asArray[self::ORDER_ITEMS] = $this->collectionToArray($this->getOrderItems());
    $asArray[self::PAYMENT] = $this->getPayment()->toDeepArray();
    $asArray[self::PRODUCTS] = $this->collectionToArray($this->getProducts());

    return $asArray;
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::ORDER_ID]) && $this->setOrderId($properties[self::ORDER_ID]);
    isset($properties[self::CUSTOMER_ID]) && $this->setCustomerId($properties[self::CUSTOMER_ID]);
    isset($properties[self::PURCHASE_ID]) && $this->setPurchaseId($properties[self::PURCHASE_ID]);
    isset($properties[self::BILLING_ADDRESS]) && $this->setBillingAddress($properties[self::BILLING_ADDRESS]);
    isset($properties[self::SHIPPING_ADDRESS]) && $this->setShippingAddress($properties[self::SHIPPING_ADDRESS]);
    isset($properties[self::PRICE_TO_PAY]) && $this->setPriceToPay($properties[self::PRICE_TO_PAY]);
    isset($properties[self::NET_PRICE]) && $this->setNetPrice($properties[self::NET_PRICE]);
    isset($properties[self::TAX_VALUE]) && $this->setTaxValue($properties[self::TAX_VALUE]);
    isset($properties[self::DISCOUNT]) && $this->setDiscount($properties[self::DISCOUNT]);
    isset($properties[self::TAX_RATE_TOTALS]) && $this->setTaxRateTotals($properties[self::TAX_RATE_TOTALS]);
    isset($properties[self::VOUCHER_CODE]) && $this->setVoucherCode($properties[self::VOUCHER_CODE]);
    isset($properties[self::COUNTRY_CODE]) && $this->setCountryCode($properties[self::COUNTRY_CODE]);
    isset($properties[self::LOCALE]) && $this->setLocale($properties[self::LOCALE]);
    isset($properties[self::STATUS]) && $this->setStatus($properties[self::STATUS]);
    isset($properties[self::ORDER_ITEMS]) && $this->setOrderItems($properties[self::ORDER_ITEMS]);
    isset($properties[self::PAYMENT]) && $this->setPayment($properties[self::PAYMENT]);
    isset($properties[self::PAYMENT_DETAILS]) && $this->setPaymentDetails($properties[self::PAYMENT_DETAILS]);
    isset($properties[self::PRODUCTS]) && $this->setProducts($properties[self::PRODUCTS]);
  }

  public function fieldKeys(): array
  {
    return [
      self::ORDER_ID => true,
      self::CUSTOMER_ID => true,
      self::PURCHASE_ID => true,
      self::BILLING_ADDRESS => true,
      self::SHIPPING_ADDRESS => true,
      self::PRICE_TO_PAY => true,
      self::NET_PRICE => true,
      self::TAX_VALUE => true,
      self::DISCOUNT => true,
      self::TAX_RATE_TOTALS => true,
      self::VOUCHER_CODE => true,
      self::COUNTRY_CODE => true,
      self::LOCALE => true,
      self::STATUS => true,
      self::ORDER_ITEMS => true,
      self::PAYMENT => true,
      self::PAYMENT_DETAILS => true,
      self::PRODUCTS => true,
    ];
  }
}
