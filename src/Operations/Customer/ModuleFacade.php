<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer;

use Vijoni\Operations\Shared\Address;
use Vijoni\Unit\BaseModuleFacade;

/**
 * @method ModuleFactory moduleFactory()
 */
class ModuleFacade extends BaseModuleFacade
{
  /**
   * @return Address[]
   */
  public function findAddressesByIds(array $addressIds): array
  {
    $useCase = $this->moduleFactory()->newFindAddressesUseCase();

    return $useCase->findAddressesByIds($addressIds);
  }

  /**
   * @return Address[]
   */
  public function findCustomerAddressesByCustomerId(string $customerId): array
  {
    $useCase = $this->moduleFactory()->newFindCustomerAddressesUseCase();

    return $useCase->findCustomerAddressesByCustomerId($customerId);
  }
}
