<?php

declare(strict_types=1);

namespace Vijoni\Application;

class Result
{
  /**
   * @param Error[] $errors
   */
  public function __construct(
    private bool $isSuccess = false,
    private array $errors = [],
    private array $details = []
  ) {
  }

  public function hasFailed(): bool
  {
    return !$this->isSuccess;
  }

  public function isSuccess(): bool
  {
    return $this->isSuccess;
  }

  public function addError(Error $error): static
  {
    $this->errors[] = $error;

    return $this;
  }

  /**
   * @return Error[]
   */
  public function getErrors(): array
  {
    return $this->errors;
  }

  public function setSuccess(): static
  {
    $this->isSuccess = true;

    return $this;
  }

  public function setFailed(): static
  {
    $this->isSuccess = false;

    return $this;
  }

  public function getFirstError(): Error
  {
    $errors = $this->getErrors();
    if (isset($errors[0])) {
      return $errors[0];
    }

    return new Error();
  }

  public function errorsToArray(): array
  {
    $result = [];
    foreach ($this->getErrors() as $error) {
      $result[$error->getErrorCode()] = $error->getDetails();
    }

    return $result;
  }

  public function getDetails(): array
  {
    return $this->details;
  }

  public function addDetails(array $details): void
  {
    $this->details = array_merge_recursive($this->details, $details);
  }
}
