<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared;

class ProductLocalizedDbTable
{
  public const DE = 'de';
  public const GB = 'gb';
  public const DE_DE = 'de_de';
  public const EN_GB = 'en_gb';

  public const KEY_PRICE = 'price';
  public const KEY_ACTIVE = 'active';
  public const KEY_DISPLAY_NAME = 'displayName';
  public const KEY_TAX_RATE_TYPE = 'taxRateType';
}
