<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Service;

use Vijoni\Sales\Checkout\Repository\CheckoutCacheRepository;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\Totals;

class CheckoutChecksum
{
  public function __construct(
    private CheckoutCacheRepository $cacheRepository
  ) {
  }

  /**
   * @param Product[] $products
   */
  public function createChecksum(string $customerId, array $products): string
  {
    $content = $customerId;
    foreach ($products as $product) {
      $content .= $product->getProductId() . $product->getPriceToPay() . $product->getCount();
    }

    return sha1($content);
  }

  public function storeChecksum(string $key, string $checksum): void
  {
    $this->cacheRepository->writeCheckoutChecksum($key, $checksum);
  }

  public function removeChecksum(string $key): void
  {
    $this->cacheRepository->removeCheckoutChecksum($key);
  }

  public function verifyChecksum(string $key, string $customerId, array $products): bool
  {
    $storedChecksum = $this->cacheRepository->readCheckoutChecksum($key);

    $checksum = $this->createChecksum(
      $customerId,
      $products
    );

    return $storedChecksum === $checksum;
  }
}
