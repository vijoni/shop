<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Repository;

use Vijoni\Operations\Shared\Address;
use Vijoni\Operations\Shared\Customer;
use Vijoni\Database\Client\DatabaseClient;

class CustomerReadRepository
{
  public function __construct(private DatabaseClient $db, private MapperFactory $mapperFactory)
  {
  }

  public function findCustomerById(string $customerId): Customer
  {
    $qb = $this->db->newQueryBuilder(
      'SELECT * FROM operations_customer WHERE customer_id = %s',
      [$customerId],
      [],
      "operations: find customer by id; customerId:[{$customerId}]"
    );
    $result = $this->db->querySingleRow($qb);

    if ($result->notFound()) {
      return new Customer();
    }

    $customerMapper = $this->mapperFactory->newCustomerMapper();

    return $customerMapper->mapDbRowToCustomer((array)$result->getValue());
  }

  public function findCustomerByEmail(string $email): Customer
  {
    $qb = $this->db->newQueryBuilder(
      'SELECT * FROM operations_customer WHERE email = %s',
      [$email],
      [],
      "operations: find customer by email; email:[{$email}]"
    );
    $result = $this->db->querySingleRow($qb);

    if ($result->notFound()) {
      return new Customer();
    }

    $customerMapper = $this->mapperFactory->newCustomerMapper();

    return $customerMapper->mapDbRowToCustomer((array)$result->getValue());
  }

  /**
   * @return Address[]
   */
  public function findAddressesByIds(array $addressIds): array
  {
    $joinedAddressIds = implode(',', $addressIds);

    $sql = <<<SQL
SELECT
  a.*, ca.type
  FROM operations_address a
      JOIN operations_customer_address ca ON a.dbid = ca.address_dbid
  WHERE address_id IN (%ls)
  ORDER BY a.updated_at;
SQL;

    $commentTpl = 'operations: find addresses by id; addressIds:[%s]';
    $qb = $this->db->newQueryBuilder(
      $sql,
      [$addressIds],
      [],
      sprintf($commentTpl, $joinedAddressIds)
    );

    $addresses = $this->db->query($qb);

    $addressMapper = $this->mapperFactory->newAddressMapper();

    return $addressMapper->mapDbRowsToAddresses($addresses);
  }

  /**
   * @return Address[]
   */
  public function findCustomerAddressesByCustomerId(string $customerId): array
  {
    $sql = <<<SQL
SELECT
  a.*, ca.type
  FROM operations_address a
      JOIN operations_customer_address ca ON a.dbid = ca.address_dbid
      JOIN operations_customer c ON c.dbid = ca.customer_dbid
  WHERE c.customer_id = %s
  ORDER BY a.updated_at;
SQL;

    $qb = $this->db->newQueryBuilder(
      $sql,
      [$customerId],
      [],
      "operations: find customer addresses by customer id; customerId:[{$customerId}]"
    );

    $addresses = $this->db->query($qb);

    $addressMapper = $this->mapperFactory->newAddressMapper();

    return $addressMapper->mapDbRowsToAddresses($addresses);
  }
}
