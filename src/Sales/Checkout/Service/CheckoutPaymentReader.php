<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Service;

use Vijoni\Sales\Checkout\ModuleConfig as CheckoutConfig;
use Vijoni\Sales\Finance\ModuleFacade as FinanceFacade;
use Vijoni\Sales\Shared\Payment;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\Totals;

class CheckoutPaymentReader
{
  public function __construct(
    private FinanceFacade $financeFacade,
    private CheckoutConfig $checkoutConfig
  ) {
  }

  /**
   * @param Product[] $products
   */
  public function readAvailablePayments(array $products, Totals $totals): array
  {
    $availablePaymentProviders = $this->checkoutConfig->getAvailablePaymentProviders();
    $payments = $this->financeFacade->createPayments(array_keys($availablePaymentProviders));

    if ($totals->getPriceToPay() < $this->checkoutConfig->getInstallmentsThreshold()) {
      $payments = $this->excludeInstallments($payments, $totals);
    }

    return $payments;
  }

  /**
   * @param Payment[] $payments
   * @return Payment[]
   */
  private function excludeInstallments(array $payments, Totals $totals): array
  {
    return array_filter(
      $payments,
      static fn(Payment $payment) => $payment->getType() !== Payment::TYPE_INSTALLMENTS
    );
  }
}
