<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Service;

use Vijoni\Operations\UnitFacade as OperationsFacade;
use Vijoni\Sales\Shared\Address;

class CheckoutAddressReader
{
  public function __construct(
    private OperationsFacade $operationsFacade
  ) {
  }

  public function readAvailableAddresses(string $customerId): array
  {
    $addresses = $this->operationsFacade->findCustomerAddressesByCustomerId($customerId);

    return $this->mapAllAddresses($addresses);
  }

  private function mapAllAddresses(array $addresses): array
  {
    $result = [];
    foreach ($addresses as $addressProperties) {
      $address = $this->mapAddress($addressProperties);
      $result[$address->getAddressId()] = $address;
    }

    return $result;
  }

  private function mapAddress(array $addressProperties): Address
  {
    $address = new Address();

    $address->intersect(
      [
        'addressId' => Address::ADDRESS_ID,
        'firstname' => Address::FIRSTNAME,
        'lastname' => Address::LASTNAME,
        'phone' => Address::PHONE,
        'street' => Address::STREET,
        'addressComplement' => Address::ADDRESS_COMPLEMENT,
        'city' => Address::CITY,
        'postCode' => Address::POST_CODE,
        'countryCode' => Address::COUNTRY_CODE,
        'type' => Address::TYPE,
      ],
      $addressProperties
    );

    $address->resetModifiedValues();

    return $address;
  }
}
