<?php

namespace VijoniTest;

use Codeception\Scenario;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use Vijoni\Application\DependencyProvider\DependencyProviderKeys;
use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Database\Client\PgDatabaseClient;
use Vijoni\Unit\DependencyProvider;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class IntegrationTester extends \Codeception\Actor
{
  use _generated\IntegrationTesterActions;

  private FakerGenerator $faker;

  public function __construct(Scenario $scenario)
  {
    parent::__construct($scenario);

    $this->faker = FakerFactory::create();
  }

  /**
   * Define custom actions here
   */

  public function faker(): FakerGenerator
  {
    return $this->faker;
  }

  public function shareDbClient(): DatabaseClient
  {
    /** @var PgDatabaseClient */
    return DependencyProvider::getInstance()->share(DependencyProviderKeys::MAIN_DATABASE);
  }
}
