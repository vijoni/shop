-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE product_catalog_product(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    product_id UUID NOT NULL UNIQUE DEFAULT uuid_generate_v1mc(),
    name VARCHAR NOT NULL UNIQUE,
    type VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:002
CREATE TRIGGER update_product_catalog_product_updated_at
    BEFORE UPDATE ON product_catalog_product
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
