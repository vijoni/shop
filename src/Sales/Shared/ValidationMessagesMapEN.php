<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class ValidationMessagesMapEN implements ValidationMessagesMap
{
  public function readMap(): array
  {
    return [
      'REQUIRED_FIELD::orderItems' => 'Order items are required',
      'REQUIRED_FIELD::payment' => 'Payment is required',
      'REQUIRED_FIELD::billingAddressId' => 'Billing address is required',
      'REQUIRED_FIELD::shippingAddressId' => 'Shipping address is required',

      'REQUIRED_FIELD::countryCode' => 'Country code is required',
      'REQUIRED_FIELD::locale' => 'Locale is required',

      'REQUIRED_FIELD::productId' => 'Product id is required',
      'MUST_BE_A_STRING::productId' => 'Product id must be a text value',
      'MUST_MATCH_UUID_FORMAT::productId' => 'Product id must match uuid format',

      'REQUIRED_FIELD::price' => 'Price is required',
      'MUST_BE_AN_INTEGER::price' => 'Price must be a number',
      'BELOW_MINIMUM::price' => 'Price has to be at least {minimum}',

      'REQUIRED_FIELD::count' => 'Count is required',
      'MUST_BE_AN_INTEGER::count' => 'Count must be a number',
      'BELOW_MINIMUM::count' => 'Count has to be at least {minimum}',

      'REQUIRED_FIELD::payment.provider' => 'Payment provider is required',
      'MUST_BE_A_OBJECT::provider' => 'Payment must be a object',
      'MUST_BE_A_STRING::payment.provider' => 'Payment provider must be a string',
      'MUST_BE_ACCEPTED_VALUE::payment.provider' => 'Payment provider must be one of accepted values',

      'MUST_BE_A_STRING::billingAddressId' => 'Billing address id must be a text value',
      'MUST_MATCH_UUID_FORMAT::billingAddressId' => 'Billing address id must match uuid format',

      'MUST_BE_A_STRING::shippingAddressId' => 'Shipping address id must be a text value',
      'MUST_MATCH_UUID_FORMAT::shippingAddressId' => 'Shipping address id must match uuid format',
    ];
  }
}
