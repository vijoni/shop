#!/usr/bin/env php

<?php

use Vijoni\Unit\AppConfig;

require_once $_composer_autoload_path ?? __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../tools/LiquibaseCommand.php';

function validateInput(array $argv): string
{
  if (!isset($argv[1])) {
    return 'Supported liquibase commands [updateSQL, update]';
  }

  return '';
}

$errorMsg = validateInput($argv);

if (empty($errorMsg)) {
  $liquibaseCommand = $argv[1];

  $appConfig = AppConfig::fromFile(__DIR__ . '/../config/_generated/config.php');

  $cmd = new LiquibaseCommand($appConfig);
  $cmd->init();
  $cmd->run($liquibaseCommand);
} else {
  echo "Validation error\n";
  echo $errorMsg . "\n";
}
