<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Repository;

use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Sales\Shared\CartItemDbTable;

class CheckoutWriteRepository
{
  public function __construct(private DatabaseClient $db)
  {
    $db->useWriteConnection();
  }

  public function createCartItem(string $customerId, OrderItem $orderItem): void
  {
    $orderItemProperties = $this->mapOrderItemToDbRow($customerId, $orderItem);
    $qb = $this->db->newQueryBuilder(
      'INSERT INTO sales_cart_item (%lc) VALUES(%l?);',
      [
        array_keys($orderItemProperties),
        array_values($orderItemProperties),
      ],
      [],
      "sales: create cart item; customerId:[{$customerId}], productId: [{$orderItem->getProductId()}]"
    );

    $this->db->query($qb);
  }

  public function updateCartItem(string $customerId, OrderItem $orderItem): void
  {
    $orderItemProperties = $this->mapOrderItemToDbRow($customerId, $orderItem);
    $qb = $this->db->newQueryBuilder(
      'UPDATE sales_cart_item SET %lu WHERE customer_id = %s AND product_id = %s',
      [
        $orderItemProperties,
        $customerId,
        $orderItem->getProductId(),
      ],
      [],
      "sales: update cart item; customerId:[{$customerId}] productId:[{$orderItem->getProductId()}]"
    );

    $this->db->query($qb);
  }

  /**
   * @param string[] $productIds
   */
  public function removeCartItems(string $customerId, array $productIds): void
  {
    $joinedProductIds = implode(',', $productIds);

    $qb = $this->db->newQueryBuilder(
      'DELETE FROM sales_cart_item WHERE customer_id = %s AND product_id IN(%ls)',
      [
        $customerId,
        $productIds,
      ],
      [],
      "sales: remove cart items; customerId:[{$customerId}] productIds:[{$joinedProductIds}]"
    );

    $this->db->query($qb);
  }

  public function clearCustomersCart(string $customerId): void
  {
    $qb = $this->db->newQueryBuilder(
      'DELETE FROM sales_cart_item WHERE customer_id = %s',
      [
        $customerId,
      ],
      [],
      "sales: clear customer's cart; customerId:[{$customerId}]"
    );

    $this->db->query($qb);
  }

  private function mapOrderItemToDbRow(string $customerId, OrderItem $orderItem): array
  {
    $dbValues = $orderItem->mapModifiedTo([
      OrderItem::PRODUCT_ID => CartItemDbTable::PRODUCT_ID,
      OrderItem::PRICE => CartItemDbTable::PRICE,
      OrderItem::COUNT => CartItemDbTable::COUNT,
    ]);

    $dbValues[CartItemDbTable::CUSTOMER_ID] = $customerId;

    return $dbValues;
  }
}
