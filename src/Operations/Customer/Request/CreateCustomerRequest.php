<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Request;

use Vijoni\Application\SchemaValidator\JsonSchemaValidator;
use Vijoni\Operations\Shared\ValidationMessagesMap;
use Vijoni\Operations\Shared\Customer;

class CreateCustomerRequest
{
  private const SCHEMA_FILEPATH = __DIR__ . '/schema-create-customer.json';

  private string $payload = '';

  public function __construct(
    private JsonSchemaValidator $jsonSchemaValidator,
    private ValidationMessagesMap $messagesMap
  ) {
  }

  public function use(string $payload): void
  {
    $this->payload = $payload;
  }

  public function validate(): array
  {
    return $this->jsonSchemaValidator->validate(
      $this->payload,
      self::SCHEMA_FILEPATH,
      $this->messagesMap->readMap()
    );
  }

  public function createCustomer(): Customer
  {
    $payload = (array)json_decode($this->payload, true);
    $data = (array)$payload['data'];

    /** @var string $birthDate */
    $birthDate = $data['birthDate'];

    $customer = new Customer();
    $customer->fromArray([
      Customer::FIRSTNAME => $data['firstname'],
      Customer::LASTNAME => $data['lastname'],
      Customer::EMAIL => $data['email'],
      Customer::BIRTH_DATE => new \DateTime($birthDate),
    ]);

    return $customer;
  }
}
