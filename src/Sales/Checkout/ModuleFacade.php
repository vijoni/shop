<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout;

use Vijoni\Unit\BaseModuleFacade;

class ModuleFacade extends BaseModuleFacade
{
}
