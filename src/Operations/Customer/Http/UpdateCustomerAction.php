<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Http;

use Vijoni\Application\Http\JsonResponse;
use Vijoni\Application\Result;
use Vijoni\Operations\Customer\ModuleFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Operations\Customer\UseCase\UpdateCustomerUseCase;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class UpdateCustomerAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $moduleFactory = $this->moduleFactory();

    $updateCustomerRequest = $moduleFactory->newUpdateCustomerRequest();
    $updateCustomerRequest->use($request->getBody()->getContents());
    $errors = $updateCustomerRequest->validate();

    if (!empty($errors)) {
      return new JsonResponse(400, [], own_json_encode($errors));
    }

    $customer = $updateCustomerRequest->createCustomer();
    $result = $moduleFactory->newUpdateCustomerUseCase()->updateCustomer($customer);

    if ($this->isNotFound($result)) {
      return new JsonResponse(404, [], '{}');
    }

    return new JsonResponse(200, [], '{}');
  }

  private function isNotFound(Result $result): bool
  {
    if ($result->isSuccess()) {
      return false;
    }

    $errorCode = $result->getFirstError()->getErrorCode();

    return $errorCode === UpdateCustomerUseCase::ERROR_NOT_FOUND;
  }
}
