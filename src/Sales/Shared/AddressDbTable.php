<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class AddressDbTable
{
  public const ORDER_DBID = 'order_dbid';
  public const FIRSTNAME = 'firstname';
  public const LASTNAME = 'lastname';
  public const PHONE = 'phone';
  public const STREET = 'street';
  public const ADDRESS_COMPLEMENT = 'address_complement';
  public const CITY = 'city';
  public const POST_CODE = 'post_code';
  public const COUNTRY_CODE = 'country_code';
  public const TYPE = 'type';
}
