<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment\Zab;

class ZabFirstPayment
{
  public function __construct(
    public string $monthlyInterest,
    public string $remainingPrice
  ) {
  }
}
