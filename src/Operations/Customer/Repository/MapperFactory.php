<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Repository;

class MapperFactory
{
  public function newCustomerMapper(): CustomerMapper
  {
    return new CustomerMapper();
  }

  public function newAddressMapper(): AddressMapper
  {
    return new AddressMapper();
  }
}
