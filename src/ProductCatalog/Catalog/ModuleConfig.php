<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog;

use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
}
