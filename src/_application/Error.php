<?php

declare(strict_types=1);

namespace Vijoni\Application;

class Error
{
  public function __construct(
    private string $errorCode = '',
    private array $details = []
  ) {
  }

  public function getErrorCode(): string
  {
    return $this->errorCode;
  }

  public function getDetails(): array
  {
    return $this->details;
  }
}
