<?php

declare(strict_types=1);

namespace Vijoni\Tech\Heartbeat;

use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
}
