-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE product_catalog_product_localized(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    product_dbid UUID NOT NULL UNIQUE REFERENCES product_catalog_product(dbid),
    global JSONB DEFAULT '{}' NOT NULL,
    de JSONB DEFAULT '{}' NOT NULL,
    gb JSONB DEFAULT '{}' NOT NULL,
    de_de JSONB DEFAULT '{}' NOT NULL,
    en_gb JSONB DEFAULT '{}' NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:002
CREATE TRIGGER update_product_catalog_product_localized_updated_at
    BEFORE UPDATE ON product_catalog_product_localized
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
