<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared;

class Product extends Generated\GeneratedProduct
{
  public function __construct()
  {
    $this->setChildren(new ProductChildCollection());
    $this->setChildProducts(new ProductCollection());
  }

  public function toDeepArray(): array
  {
    $asArray = parent::toDeepArray();
    $asArray[self::CHILDREN] = $this->getChildren()->toDeepArray();
    $asArray[self::CHILD_PRODUCTS] = $this->getChildProducts()->toDeepArray();

    return $asArray;
  }

  public function clone(): Product
  {
    $clone = clone $this;

    $this->cloneChildProductsFrom($this, $clone);

    return $clone;
  }

  private function cloneChildProductsFrom(Product $source, Product $target): void
  {
    $productCollection = new ProductCollection();
    foreach ($source->getChildProducts() as $product) {
      $productId = $product->getProductId();
      $productCollection[$productId] = $product->clone();
    }

    $target->setChildProducts($productCollection);
  }

  public function isTypeSet(): bool
  {
    return $this->getType() === self::TYPE_SET;
  }
}
