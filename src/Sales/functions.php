<?php

declare(strict_types=1);

function rawTaxRateToPercentageTaxRate(string $taxRate): string
{
  return bcdiv($taxRate, '100', 2);
}
