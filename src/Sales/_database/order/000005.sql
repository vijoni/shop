-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_order_payment(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    order_dbid UUID NOT NULL REFERENCES sales_order(dbid),
    provider VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    variant VARCHAR NOT NULL,
    details JSONB NOT NULL DEFAULT '{}',
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:002
CREATE INDEX IF NOT EXISTS sales_order_payment__provider ON sales_order_payment USING BTREE (provider);
CREATE INDEX IF NOT EXISTS sales_order_payment__type ON sales_order_payment USING BTREE (type);

-- changset kgawlinski:003
CREATE TRIGGER update_sales_order_payment_updated_at
    BEFORE UPDATE ON sales_order_payment
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
