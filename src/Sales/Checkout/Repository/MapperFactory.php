<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Repository;

class MapperFactory
{
  public function newOrderItemMapper(): OrderItemMapper
  {
    return new OrderItemMapper();
  }
}
