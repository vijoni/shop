<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog\Repository;

use Vijoni\ProductCatalog\Shared\ProductChild;
use Vijoni\ProductCatalog\Shared\ProductChildCollection;
use Vijoni\ProductCatalog\Shared\ProductCollection;
use Vijoni\ProductCatalog\Shared\ProductDbTable;
use Vijoni\ProductCatalog\Shared\ProductLocalizedDbTable;
use Vijoni\ProductCatalog\Shared\Product;

class ProductMapper
{
  public function mapDbRowsToProducts(\Traversable $rows): ProductCollection
  {
    $productCollection = new ProductCollection();
    foreach ($rows as $row) {
      $productId = $row[ProductDbTable::PRODUCT_ID];
      $product = $this->mapDbRowToProduct($row);
      $productCollection[$productId] = $product;
    }

    return $productCollection;
  }

  public function mapDbRowToProduct(array $row): Product
  {
    $product = new Product();
    $product->fromArray([
      Product::PRODUCT_ID => $row[ProductDbTable::PRODUCT_ID],
      Product::NAME => $row[ProductDbTable::NAME],
      Product::TYPE => $row[ProductDbTable::TYPE],
      Product::COUNT => 1,
    ]);

    $product->intersect(
      [
        ProductLocalizedDbTable::KEY_ACTIVE => Product::IS_ACTIVE,
        ProductLocalizedDbTable::KEY_DISPLAY_NAME => Product::DISPLAY_NAME,
        ProductLocalizedDbTable::KEY_TAX_RATE_TYPE => Product::TAX_RATE_TYPE,
        ProductLocalizedDbTable::KEY_PRICE => Product::PRICE,
      ],
      throwable_json_decode($row[ProductDbTable::ALIAS_ATTRIBUTES], true)
    );

    $childCollection = $this->createChildren($row);
    $product->setChildren($childCollection);

    return $product;
  }

  private function createChildren(array $row): ProductChildCollection
  {
    $childrenCollection = new ProductChildCollection();
    $childrenCountMap = $this->combineChildrenCount($row);

    foreach ($childrenCountMap as $productId => $count) {
      $productChild = new ProductChild();
      $productChild->setProductId($productId);
      $productChild->setCount((int)$count);

      $childrenCollection->offsetSet($productId, $productChild);
    }

    return $childrenCollection;
  }

  private function combineChildrenCount(array $row): array
  {
    if (empty($row[ProductDbTable::ALIAS_CHILDREN_ID])) {
      return [];
    }

    $childrenIds = empty_explode(',', $row[ProductDbTable::ALIAS_CHILDREN_ID]);
    $childrenCounts = empty_explode(',', $row[ProductDbTable::ALIAS_CHILDREN_COUNT]);

    return array_combine($childrenIds, $childrenCounts);
  }
}
