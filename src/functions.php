<?php

declare(strict_types=1);

function iter_map(Traversable $traversable, callable $callback): array
{
  $result = [];
  foreach ($traversable as $key => $value) {
    $result[$key] = $callback($value);
  }

  return $result;
}

function uuidv4(): string
{
  return sprintf(
    '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    // 32 bits for the time_low
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff),
    // 16 bits for the time_mid
    mt_rand(0, 0xffff),
    // 16 bits for the time_hi,
    mt_rand(0, 0x0fff) | 0x4000,
    // 8 bits and 16 bits for the clk_seq_hi_res,
    // 8 bits for the clk_seq_low,
    mt_rand(0, 0x3fff) | 0x8000,
    // 48 bits for the node
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff),
    mt_rand(0, 0xffff)
  );
}

/**
 * @param int<1, max> $depth
 */
function own_json_encode(mixed $value, int $flags = 0, int $depth = 512): string
{
  return json_encode($value, $flags | JSON_THROW_ON_ERROR, $depth);
}

/**
 * @param int<1, max> $depth
 */
function throwable_json_decode(string $json, ?bool $associative = false, int $depth = 512, int $flags = 0): array
{
  return (array)json_decode($json, $associative, $depth, $flags | JSON_THROW_ON_ERROR);
}

function empty_explode(string $separator, string $string, int $limit = PHP_INT_MAX): array
{
  if ($separator === '') {
    throw new RuntimeException('Empty explode separator');
  }

  if ($string === '') {
    return [];
  }

  return explode($separator, $string, $limit);
}

function bcround(string $number, int $precision = 0): string
{
  if (str_contains($number, '.') === false) {
    return $number;
  }

  if ($number[0] === '-') {
    return bcsub($number, '0.' . str_repeat('0', $precision) . '5', $precision);
  }

  return bcadd($number, '0.' . str_repeat('0', $precision) . '5', $precision);
}

function roundCents(string $number): string
{
  return bcround($number, 0);
}

function nullifyEmptyString(string $value): ?string
{
  return $value === '' ? null : $value;
}
