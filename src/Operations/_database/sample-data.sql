INSERT INTO operations_customer
(customer_id, firstname, lastname, email, birth_date)
VALUES
    ('2ade4a2e-e889-11ec-aadf-bbbc23c59e8c', 'Tom', 'Hanks', 'tom.hanks@sunshinesmile.de', '1951-03-09'),
    ('2ade5596-e889-11ec-aadf-bf117f00cddc', 'Ossama', 'van Malsen', 'ossama.malsen@sunshinesmile.de', '1944-05-12')
;

INSERT INTO operations_address
(address_id, firstname, lastname, phone, street, address_complement, city, post_code, country_code)
VALUES
    ('bb9f8f4a-f39d-11ec-a76c-3731dfaddcc7','Tom', 'Hanks', '0421109741', 'Grolmanstraße 75', null, 'Bremen Seehausen', '28197', 'de'),
    ('bb9f9878-f39d-11ec-a76c-570730e716d6', 'Tom', 'Hanks', '0421109741', 'Neuer Jungfernstieg 11', null, 'Adlkofen', '84166', 'de'),
    ('bb9f9972-f39d-11ec-a76c-6b73b8db936d','Ossama', 'van Malsen', '0690062948', 'Lijndonk 64', null, 'Oosterhout', '4907 XE', 'nl')
;

INSERT INTO operations_customer_address (customer_dbid, address_dbid, type) (
    SELECT c.dbid, a.dbid, 'billing'
        FROM operations_customer c JOIN operations_address a
        ON c.email = 'tom.hanks@sunshinesmile.de' AND a.street = 'Grolmanstraße 75'
);

INSERT INTO operations_customer_address (customer_dbid, address_dbid, type) (
    SELECT c.dbid, a.dbid, 'shipping'
    FROM operations_customer c JOIN operations_address a
        ON c.email = 'tom.hanks@sunshinesmile.de' AND a.street = 'Neuer Jungfernstieg 11'
);



