<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class OrderDbTable
{
  public const ORDER_ID = 'order_id';
  public const CUSTOMER_ID = 'customer_id';
  public const PURCHASE_ID = 'purchase_id';
  public const PRICE_TO_PAY = 'price_to_pay';
  public const NET_PRICE = 'net_price';
  public const TAX_VALUE = 'tax_value';
  public const DISCOUNT = 'discount';
  public const VOUCHER_CODE = 'voucher_code';
  public const COUNTRY_CODE = 'country_code';
  public const LOCALE = 'locale';
  public const STATUS = 'status';
  public const CREATED_AT = 'created_at';
}
