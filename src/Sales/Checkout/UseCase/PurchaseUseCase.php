<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\UseCase;

use Vijoni\Application\Error;
use Vijoni\Sales\Checkout\Service\CheckoutChecksum;
use Vijoni\Sales\Checkout\Service\CheckoutReaderAggregate;
use Vijoni\Sales\Finance\ModuleFacade as FinanceFacade;
use Vijoni\Sales\Order\ModuleFacade as OrderFacade;
use Vijoni\Sales\Shared\InstallmentPayment;
use Vijoni\Sales\Shared\Order;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\Region;
use Vijoni\Sales\Shared\Result\PurchaseUseCaseResult;
use Vijoni\Sales\Shared\Totals;

class PurchaseUseCase
{
  public const PURCHASE_CHECKOUT_REQUEST_MISMATCH = 'PURCHASE_CHECKOUT_REQUEST_MISMATCH';
  public const DUPLICATE_ORDER = 'DUPLICATE_ORDER';

  public function __construct(
    private CheckoutReaderAggregate $checkoutReader,
    private CheckoutChecksum $checkoutChecksum,
    private FinanceFacade $financeFacade,
    private OrderFacade $orderFacade
  ) {
  }

  public function purchase(Order $order, string $purchaseTransactionId): PurchaseUseCaseResult
  {
    $this->loadProducts($order);
    $customerId = $order->getCustomerId();

    try {
      $this->verifyPurchaseDuplicate($customerId, $purchaseTransactionId);
      $this->verifyChecksum($customerId, $order->getProducts());
      $order->setPurchaseId($purchaseTransactionId);
      $this->applyOrderTotals($order);
    } catch (PurchaseUseCaseException $e) {
      return $e->getUseCaseResult();
    }

    $order->setStatus(Order::STATUS_WAITING_FOR_PAYMENT);
    $this->orderFacade->createOrder($order);
    $this->checkoutChecksum->removeChecksum($customerId);

    return new PurchaseUseCaseResult(true);
  }

  private function loadProducts(Order $order): void
  {
    $region = new Region($order->getCountryCode(), $order->getLocale());

    $products = $this->checkoutReader->getProductReader()->readProducts(
      $order->getOrderItems(),
      $region
    );

    $this->checkoutReader->getProductReader()->mergeOrderItemsPropertiesIntoProducts(
      $order->getOrderItems(),
      $products
    );

    $order->setProducts($products);
  }

  /**
   * @param Product[] $products
   * @throws PurchaseUseCaseException
   */
  private function verifyChecksum(string $customerId, array $products): void
  {
    $matchOrder = $this->checkoutChecksum->verifyChecksum($customerId, $customerId, $products);
    if (!$matchOrder) {
      $result = new PurchaseUseCaseResult(false, [new Error(self::PURCHASE_CHECKOUT_REQUEST_MISMATCH)]);
      throw new PurchaseUseCaseException($result);
    }
  }

  /**
   * @throws PurchaseUseCaseException
   */
  private function verifyPurchaseDuplicate(string $customerId, string $purchaseTransactionId): void
  {
    $keyAlreadyExists = $this->orderFacade->tryMarkPurchaseDuplicate($customerId, $purchaseTransactionId);

    if (!$keyAlreadyExists) {
      $result = new PurchaseUseCaseResult(false, [new Error(self::DUPLICATE_ORDER)]);
      throw new PurchaseUseCaseException($result);
    }
  }

  private function applyOrderTotals(Order $order): void
  {
    $this->financeFacade->calculateAllProductSums($order->getProducts());

    $taxRateTotals = $this->financeFacade->calculateTotalsPerTaxRate($order->getProducts());
    $totals = $this->financeFacade->calculateTotalsFromTaxRateTotals($taxRateTotals);

    $order->setPriceToPay($totals->getPriceToPay());
    $order->setNetPrice($totals->getNetPrice());
    $order->setTaxValue($totals->getTaxValue());
    $order->setDiscount($totals->getDiscountValue());
    $order->setTaxRateTotals($taxRateTotals);
    $this->applyOrderPaymentDetails($order, $totals);
  }

  private function applyOrderPaymentDetails(Order $order, Totals $totals): void
  {
    $payment = $order->getPayment();
    if ($payment instanceof InstallmentPayment) {
      $variantTotals = $payment->calculateVariant($totals, $payment->getVariant());
      $order->setPaymentDetails($variantTotals->toArray());
    }
  }
}
