<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class OrderPaymentDbTable
{
  public const ORDER_DBID = 'order_dbid';
  public const PROVIDER = 'provider';
  public const TYPE = 'type';
  public const VARIANT = 'variant';
  public const DETAILS = 'details';
}
