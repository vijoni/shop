<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment;

use Vijoni\Sales\Shared\Payment;

class StripeCreditCardPayment extends Payment
{
  public function __construct()
  {
    parent::__construct(Payment::TYPE_ONETIME_PAYMENT, Payment::PROVIDER_STRIPE_CREDIT_CARD);
  }
}
