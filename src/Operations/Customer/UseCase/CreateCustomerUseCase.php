<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\UseCase;

use Vijoni\Application\Error;
use Vijoni\Application\Result;
use Vijoni\Operations\Customer\Repository\CustomerReadWriteRepository;
use Vijoni\Operations\Shared\Customer;

class CreateCustomerUseCase
{
  public const ERROR_DUPLICATE_ENTRY = 'ERROR_DUPLICATE_ENTRY';

  public function __construct(private CustomerReadWriteRepository $customerRepository)
  {
  }

  public function createCustomer(Customer $customer): Result
  {
    $email = $customer->getEmail();
    $existingCustomer = $this->customerRepository->read()->findCustomerByEmail($email);
    if ($existingCustomer->hasId()) {
      return new Result(false, [new Error(self::ERROR_DUPLICATE_ENTRY, ['email' => $email])]);
    }

    $this->customerRepository->write()->createCustomer($customer);

    return new Result(true);
  }
}
