<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Repository;

use Vijoni\Sales\Shared\CartItemDbTable;
use Vijoni\Sales\Shared\OrderItem;

class OrderItemMapper
{
  public function mapDbRowsToOrderItems(\Generator $rows): array
  {
    $orderItem = [];
    foreach ($rows as $row) {
      $orderItem[] = $this->mapDbRowToOrderItem((array)$row);
    }

    return $orderItem;
  }

  public function mapDbRowToOrderItem(array $row): OrderItem
  {
    $orderItem = new OrderItem();
    $orderItem->fromArray([
      OrderItem::PRODUCT_ID => $row[CartItemDbTable::PRODUCT_ID],
      OrderItem::PRICE => (int)$row[CartItemDbTable::PRICE],
      OrderItem::COUNT => (int)$row[CartItemDbTable::COUNT],
    ]);

    return $orderItem;
  }
}
