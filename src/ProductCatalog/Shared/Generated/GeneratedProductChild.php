<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedProductChild extends ClassBase
{
  private string $productId;
  private int $count = -1;

  public const PRODUCT_ID = 'productId';
  public const COUNT = 'count';

  public function getProductId(): string
  {
    return $this->productId;
  }

  public function setProductId(string $productId): void
  {
    $this->productId = $productId;
    $this->modified[self::PRODUCT_ID] = true;
  }

  public function getCount(): int
  {
    return $this->count;
  }

  public function setCount(int $count): void
  {
    $this->count = $count;
    $this->modified[self::COUNT] = true;
  }

  public function toArray(): array
  {
    return [
      self::PRODUCT_ID => $this->getProductId(),
      self::COUNT => $this->getCount(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::PRODUCT_ID]) && $this->setProductId($properties[self::PRODUCT_ID]);
    isset($properties[self::COUNT]) && $this->setCount($properties[self::COUNT]);
  }

  public function fieldKeys(): array
  {
    return [
      self::PRODUCT_ID => true,
      self::COUNT => true,
    ];
  }
}
