<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Request;

use Vijoni\Application\SchemaValidator\JsonSchemaValidator;
use Vijoni\Operations\UnitFacade as OperationsUnitFacade;
use Vijoni\Sales\Finance\ModuleFacade as FinanceFacade;
use Vijoni\Sales\Shared\Address;
use Vijoni\Sales\Shared\Order;
use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Sales\Shared\ValidationMessagesMap;

class PurchaseRequest
{
  private const SCHEMA_FILEPATH = __DIR__ . '/schema-purchase.json';

  private string $payload = '';
  private string $customerId = '';

  public function __construct(
    private JsonSchemaValidator $jsonSchemaValidator,
    private ValidationMessagesMap $messagesMap,
    private FinanceFacade $financeFacade,
    private OperationsUnitFacade $operationsUnitFacade
  ) {
  }

  public function use(string $customerId, string $payload): void
  {
    $this->customerId = $customerId;
    $this->payload = $payload;
  }

  public function validate(): array
  {
    return $this->jsonSchemaValidator->validate(
      $this->payload,
      self::SCHEMA_FILEPATH,
      $this->messagesMap->readMap()
    );
  }

  public function createOrder(): Order
  {
    $payload = (array)json_decode($this->payload, true);
    $data = (array)$payload['data'];

    $order = new Order();
    $order->setCustomerId($this->customerId);
    $this->applyOrderItems($order, $data);
    $this->applyPayment($order, $data);
    $this->applyRegion($order, $data);
    $this->applyAddresses($order, $data);

    return $order;
  }

  private function createOrderItem(array $orderItemAttributes): OrderItem
  {
    $orderItem = new OrderItem();
    $orderItem->fromArray([
      OrderItem::PRODUCT_ID => $orderItemAttributes['productId'],
      OrderItem::PRICE => (string)$orderItemAttributes['price'],
      OrderItem::COUNT => $orderItemAttributes['count'],
    ]);

    return $orderItem;
  }

  private function applyOrderItems(Order $order, array $data): void
  {
    $orderItems = array_map($this->createOrderItem(...), (array)$data['orderItems']);
    $order->setOrderItems($orderItems);
  }

  private function applyPayment(Order $order, array $data): void
  {
    $paymentAttributes = (array)$data['payment'];
    $providerCode = $paymentAttributes['provider'];
    $payments = $this->financeFacade->createPayments([$providerCode]);

    $payment = $payments[$providerCode];
    $payment->setVariant($paymentAttributes['variant']);

    $order->setPayment($payment);
  }

  private function applyRegion(Order $order, array $data): void
  {
    /** @var string */
    $countryCode = $data['countryCode'];

    /** @var string */
    $locale = $data['locale'];

    $order->setCountryCode($countryCode);
    $order->setLocale($locale);
  }

  private function applyAddresses(Order $order, array $data): void
  {
    /** @var string */
    $billingAddressId = $data['billingAddressId'];

    /** @var string */
    $shippingAddressId = $data['shippingAddressId'];

    $addresses = $this->operationsUnitFacade->findAddressesByIds([$billingAddressId, $shippingAddressId]);
    $order->setBillingAddress($this->extractAddress($addresses, $billingAddressId, Address::TYPE_BILLING));
    $order->setShippingAddress($this->extractAddress($addresses, $shippingAddressId, Address::TYPE_SHIPPING));
  }

  private function extractAddress(array $addresses, string $addressId, string $addressType): Address
  {
    $addressProperties = $addresses[$addressId];
    $address = $this->createAddress($addressProperties);
    $address->setType($addressType);

    return $address;
  }

  private function createAddress(array $addressAttributes): Address
  {
    $address = new Address();
    $address->fromArray([
      Address::ADDRESS_ID => $addressAttributes['addressId'],
      Address::FIRSTNAME => $addressAttributes['firstname'],
      Address::LASTNAME => $addressAttributes['lastname'],
      Address::PHONE => $addressAttributes['phone'],
      Address::STREET => $addressAttributes['street'],
      Address::ADDRESS_COMPLEMENT => $addressAttributes['addressComplement'],
      Address::CITY => $addressAttributes['city'],
      Address::POST_CODE => $addressAttributes['postCode'],
      Address::COUNTRY_CODE => $addressAttributes['countryCode'],
      Address::TYPE => $addressAttributes['type'],
    ]);

    return $address;
  }

  public function getCustomerId(): string
  {
    return $this->customerId;
  }
}
