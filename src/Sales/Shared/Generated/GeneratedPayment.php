<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedPayment extends ClassBase
{
  private string $type = '';
  private string $provider = '';
  private string $variant = '';

  public const TYPE = 'type';
  public const PROVIDER = 'provider';
  public const VARIANT = 'variant';

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): void
  {
    $this->type = $type;
    $this->modified[self::TYPE] = true;
  }

  public function getProvider(): string
  {
    return $this->provider;
  }

  public function setProvider(string $provider): void
  {
    $this->provider = $provider;
    $this->modified[self::PROVIDER] = true;
  }

  public function getVariant(): string
  {
    return $this->variant;
  }

  public function setVariant(string $variant): void
  {
    $this->variant = $variant;
    $this->modified[self::VARIANT] = true;
  }

  public function toArray(): array
  {
    return [
      self::TYPE => $this->getType(),
      self::PROVIDER => $this->getProvider(),
      self::VARIANT => $this->getVariant(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::TYPE]) && $this->setType($properties[self::TYPE]);
    isset($properties[self::PROVIDER]) && $this->setProvider($properties[self::PROVIDER]);
    isset($properties[self::VARIANT]) && $this->setVariant($properties[self::VARIANT]);
  }

  public function fieldKeys(): array
  {
    return [
      self::TYPE => true,
      self::PROVIDER => true,
      self::VARIANT => true,
    ];
  }
}
