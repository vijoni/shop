<?php

declare(strict_types=1);

namespace Vijoni\Operations;

use Vijoni\Operations\Customer\ModuleFacade as CustomerFacade;
use Vijoni\Operations\Shared\Address;
use Vijoni\Unit\BaseUnitFacade;

class UnitFacade extends BaseUnitFacade
{
  public function findCustomerAddressesByCustomerId(string $customerId): array
  {
    /** @var CustomerFacade $moduleFacade */
    $moduleFacade = $this->dependencyProvider()->shareModuleFacade(CustomerFacade::class);
    $addresses = $moduleFacade->findCustomerAddressesByCustomerId($customerId);

    return $this->exposeAddresses($addresses);
  }

  public function findAddressesByIds(array $addressIds): array
  {
    /** @var CustomerFacade $moduleFacade */
    $moduleFacade = $this->dependencyProvider()->shareModuleFacade(CustomerFacade::class);
    $addresses = $moduleFacade->findAddressesByIds($addressIds);

    return $this->exposeAddresses($addresses);
  }

  /**
   * @param Address[] $addresses
   */
  private function exposeAddresses(array $addresses): array
  {
    $exposedAddresses = [];
    foreach ($addresses as $address) {
      $exposedAddresses[$address->getAddressId()] = $address->toArray();
    }

    return $exposedAddresses;
  }
}
