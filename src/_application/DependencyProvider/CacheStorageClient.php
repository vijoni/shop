<?php

declare(strict_types=1);

namespace Vijoni\Application\DependencyProvider;

use Vijoni\Application\Redis\RedisClient;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
trait CacheStorageClient
{
  /**
   * @return RedisClient
   */
  public function shareCacheStorageClient(): RedisClient
  {
    /** @var RedisClient */
    return $this->dependencyProvider()->share(DependencyProviderKeys::CACHE_STORAGE);
  }
}
