<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog;

use Vijoni\ProductCatalog\Shared\ProductCollection;
use Vijoni\ProductCatalog\Shared\Region;
use Vijoni\Unit\BaseModuleFacade;

/**
 * @method ModuleFactory moduleFactory()
 */
class ModuleFacade extends BaseModuleFacade
{
  public function findProductsByIdByRegion(array $productIds, Region $region): ProductCollection
  {
    $useCase = $this->moduleFactory()->newFindProductsUseCase();

    return $useCase->findProductsByIdByRegion($productIds, $region);
  }
}
