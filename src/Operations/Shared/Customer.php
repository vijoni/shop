<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared;

use Vijoni\Operations\Shared\Generated\GeneratedCustomer;

class Customer extends GeneratedCustomer
{
  public function __construct()
  {
    $this->setBirthDate(new \DateTime(self::DEFAULT_DATE));
    $this->resetModifiedValues();
  }

  public function hasId(): bool
  {
    return $this->getCustomerId() !== '';
  }
}
