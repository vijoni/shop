<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared;

interface ValidationMessagesMap
{
  public function readMap(): array;
}
