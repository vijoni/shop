<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order\Repository;

use Vijoni\Sales\Shared\Address;
use Vijoni\Sales\Shared\AddressDbTable;
use Vijoni\Sales\Shared\Order;
use Vijoni\Sales\Shared\OrderDbTable;
use Vijoni\Sales\Shared\OrderItemDbTable;
use Vijoni\Sales\Shared\OrderPaymentDbTable;
use Vijoni\Sales\Shared\OrderTotalsDbTable;
use Vijoni\Sales\Shared\Payment;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\Totals;

class OrderMapper
{
  public function mapOrderToDbRow(Order $order): array
  {
    return $order->mapModifiedTo([
      Order::CUSTOMER_ID => OrderDbTable::CUSTOMER_ID,
      Order::PURCHASE_ID => OrderDbTable::PURCHASE_ID,
      Order::PRICE_TO_PAY => OrderDbTable::PRICE_TO_PAY,
      Order::NET_PRICE => OrderDbTable::NET_PRICE,
      Order::TAX_VALUE => OrderDbTable::TAX_VALUE,
      Order::DISCOUNT => OrderDbTable::DISCOUNT,
      Order::VOUCHER_CODE => OrderDbTable::VOUCHER_CODE,
      Order::COUNTRY_CODE => OrderDbTable::COUNTRY_CODE,
      Order::LOCALE => OrderDbTable::LOCALE,
      Order::STATUS => OrderDbTable::STATUS,
    ]);
  }

  public function mapOrderToOrderTotalsDbRows(Order $order, string $orderDbId): array
  {
    $orderTotalsRows = [];
    foreach ($order->getTaxRateTotals() as $taxRate => $taxRateTotal) {
      $orderTotalsRows[] = $this->mapOrderToOrderTotalsDbRow($orderDbId, $taxRate, $taxRateTotal);
    }

    return $orderTotalsRows;
  }

  private function mapOrderToOrderTotalsDbRow(string $orderDbId, string $taxRate, Totals $taxRateTotal): array
  {
    $taxRateTotalsRow = $taxRateTotal->mapModifiedTo([
      Totals::PRICE_TO_PAY => OrderTotalsDbTable::PRICE_TO_PAY,
      Totals::NET_PRICE => OrderTotalsDbTable::NET_PRICE,
      Totals::TAX_VALUE => OrderTotalsDbTable::TAX_VALUE,
    ]);

    $taxRateTotalsRow[OrderTotalsDbTable::TAX_RATE] = $taxRate;
    $taxRateTotalsRow[OrderTotalsDbTable::ORDER_DBID] = $orderDbId;

    return $taxRateTotalsRow;
  }

  public function mapOrderToOrderItemsDbRows(Order $order, string $orderDbId): array
  {
    $orderItemsRows = [];
    foreach ($order->getProducts() as $product) {
      $orderItemsRows = array_merge($orderItemsRows, $this->mapDeepProductToOrderItemDbRow($orderDbId, $product));
    }

    return $orderItemsRows;
  }

  /**
   * @return array[]
   */
  private function mapDeepProductToOrderItemDbRow(string $orderDbId, Product $product): array
  {
    $orderItems = [$this->mapProductToOrderItemDbRow($orderDbId, $product)];

    if ($product->hasChildProducts()) {
      foreach ($product->getChildProducts() as $childProduct) {
        $orderItems[] = $this->mapProductToOrderItemDbRow($orderDbId, $childProduct);
      }
    }

    return $orderItems;
  }

  private function mapProductToOrderItemDbRow(string $orderDbId, Product $product): array
  {
    $orderItemRow = $product->mapModifiedTo([
      Product::PRODUCT_ID => OrderItemDbTable::PRODUCT_ID,
      Product::PARENT_PRODUCT_ID => OrderItemDbTable::PARENT_PRODUCT_ID,
      Product::TYPE => OrderItemDbTable::PRODUCT_TYPE,
      Product::PRICE_TO_PAY => OrderItemDbTable::PRICE_TO_PAY,
      Product::NET_PRICE => OrderItemDbTable::NET_PRICE,
      Product::TAX_VALUE => OrderItemDbTable::TAX_VALUE,
      Product::DISCOUNT_VALUE => OrderItemDbTable::DISCOUNT,
      Product::COUNT => OrderItemDbTable::COUNT,
      Product::ORDERED_COUNT => OrderItemDbTable::ORDERED_COUNT,
      Product::TOTAL_PRICE_TO_PAY => OrderItemDbTable::TOTAL_PRICE_TO_PAY,
    ]);

    $orderItemRow[OrderItemDbTable::ORDER_DBID] = $orderDbId;
    //sets cannot have tax rate, children have tax rate
    $orderItemRow[OrderItemDbTable::TAX_RATE] = nullifyEmptyString($product->getTaxRate());

    return $orderItemRow;
  }

  public function mapOrderToOrderPaymentDbRows(Order $order, string $orderDbId): array
  {
    $payment = $order->getPayment();
    $paymentRow = $payment->mapModifiedTo([
      Payment::PROVIDER => OrderPaymentDbTable::PROVIDER,
      Payment::TYPE => OrderPaymentDbTable::TYPE,
      Payment::VARIANT => OrderPaymentDbTable::VARIANT,
    ]);

    $paymentRow[OrderPaymentDbTable::ORDER_DBID] = $orderDbId;
    $paymentRow[OrderPaymentDbTable::DETAILS] = $this->encodePaymentDetails($order->getPaymentDetails());

    return $paymentRow;
  }

  private function encodePaymentDetails(array $paymentDetails): string
  {
    unset($paymentDetails[Totals::NET_PRICE]);
    unset($paymentDetails[Totals::TAX_VALUE]);
    unset($paymentDetails[Totals::DISCOUNT_VALUE]);

    return own_json_encode($paymentDetails, JSON_FORCE_OBJECT);
  }

  public function mapOrderAddressToAddressDbRow(Address $address, string $orderDbId): array
  {
    $addressRow = $address->mapModifiedTo([
      Address::FIRSTNAME => AddressDbTable::FIRSTNAME,
      Address::LASTNAME => AddressDbTable::LASTNAME,
      Address::PHONE => AddressDbTable::PHONE,
      Address::STREET => AddressDbTable::STREET,
      Address::ADDRESS_COMPLEMENT => AddressDbTable::ADDRESS_COMPLEMENT,
      Address::CITY => AddressDbTable::CITY,
      Address::POST_CODE => AddressDbTable::POST_CODE,
      Address::COUNTRY_CODE => AddressDbTable::COUNTRY_CODE,
      Address::TYPE => AddressDbTable::TYPE,
    ]);

    $addressRow[OrderPaymentDbTable::ORDER_DBID] = $orderDbId;

    return $addressRow;
  }
}
