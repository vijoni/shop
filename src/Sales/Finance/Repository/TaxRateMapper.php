<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Repository;

use Vijoni\Sales\Shared\TaxRate;
use Vijoni\Sales\Shared\TaxRateDbTable;

class TaxRateMapper
{
  public function mapDbRowsToTaxRates(array $rows): array
  {
    $taxRates = [];
    foreach ($rows as $row) {
      $type = $row[TaxRateDbTable::TYPE];
      $taxRates[$type] = new TaxRate($type, $row[TaxRateDbTable::VALUE]);
    }

    return $taxRates;
  }
}
