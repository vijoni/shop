<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedOrderItem extends ClassBase
{
  private string $productId = '';
  private string $price = '';
  private int $count = -1;

  public const PRODUCT_ID = 'productId';
  public const PRICE = 'price';
  public const COUNT = 'count';

  public function getProductId(): string
  {
    return $this->productId;
  }

  public function setProductId(string $productId): void
  {
    $this->productId = $productId;
    $this->modified[self::PRODUCT_ID] = true;
  }

  public function getPrice(): string
  {
    return $this->price;
  }

  public function setPrice(string $price): void
  {
    $this->price = $price;
    $this->modified[self::PRICE] = true;
  }

  public function getCount(): int
  {
    return $this->count;
  }

  public function setCount(int $count): void
  {
    $this->count = $count;
    $this->modified[self::COUNT] = true;
  }

  public function toArray(): array
  {
    return [
      self::PRODUCT_ID => $this->getProductId(),
      self::PRICE => $this->getPrice(),
      self::COUNT => $this->getCount(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::PRODUCT_ID]) && $this->setProductId($properties[self::PRODUCT_ID]);
    isset($properties[self::PRICE]) && $this->setPrice($properties[self::PRICE]);
    isset($properties[self::COUNT]) && $this->setCount($properties[self::COUNT]);
  }

  public function fieldKeys(): array
  {
    return [
      self::PRODUCT_ID => true,
      self::PRICE => true,
      self::COUNT => true,
    ];
  }
}
