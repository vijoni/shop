<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Repository;

use Vijoni\Application\Redis\RedisClient;

class CheckoutCacheRepository
{
  private const KEY_PREFIX = 'checkout_';
  private const KEY_CHECKSUM = 'checksum_';

  private const CHECKOUT_GUARANTY_TTL = 600; // 10 minutes

  public function __construct(private RedisClient $redis)
  {
  }

  public function readCheckoutChecksum(string $key): string
  {
    $token = $this->redis->getString($this->buildKey(self::KEY_CHECKSUM . $key));
    if (is_null($token)) {
      return '';
    }

    return $token;
  }

  public function writeCheckoutChecksum(string $key, string $checksum): void
  {
    $this->redis->setEx($this->buildKey(self::KEY_CHECKSUM, $key), self::CHECKOUT_GUARANTY_TTL, $checksum);
  }

  public function removeCheckoutChecksum(string $key): void
  {
    $this->redis->unlink($this->buildKey(self::KEY_CHECKSUM, $key));
  }

  private function buildKey(string $key, string $suffix = ''): string
  {
    return self::KEY_PREFIX . $key . $suffix;
  }
}
