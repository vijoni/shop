INSERT INTO product_catalog_product (product_id, name, type) VALUES
('6ad0b1e1-9a47-4f8b-92ba-6d9ed88c92f5', 'aligners-standard', 'simple'),
('6d940e50-f8e1-4ff9-9f9f-6a2bc8d09279', 'aligners-pro', 'simple'),
('f11b5b99-44cc-43bf-a12d-0b64fb8aae8a', 'retainers', 'simple'),
('bf316728-c6fe-4480-8d5d-31440ae84741', 'retainers-subscription', 'subscription')
;

INSERT INTO product_catalog_product_localized (product_dbid, de, de_de) (
    SELECT dbid, '{"active": true, "taxRateType": "reduced", "price": "169000"}', '{"displayName": "Zahnschienen Standard", "price": "169000"}'
        FROM product_catalog_product WHERE name = 'aligners-standard'
);

INSERT INTO product_catalog_product_localized (product_dbid, de, de_de) (
    SELECT dbid, '{"active": true, "taxRateType": "standard", "price": "319000"}', '{"displayName": "Zahnschienen Pro", "price": "319000"}'
        FROM product_catalog_product WHERE name = 'aligners-pro'
);

INSERT INTO product_catalog_product_localized (product_dbid, de, de_de) (
    SELECT dbid, '{"active": true, "taxRateType": "reduced", "price": "9900"}', '{"displayName": "Retainer", "price": "14900"}'
    FROM product_catalog_product WHERE name = 'retainers'
);

--- PRODUCT SETS
INSERT INTO product_catalog_product (product_id, name, type) VALUES
    ('450ed1f4-6bd8-4086-8e85-737584945fef', 'aligners-with-retainers', 'set'),
    ('6b2ee5ad-c9d3-4de1-ae54-5279e4561d8a', 'pro-aligners-with-retainers', 'set')
;

-- aligners-with-retainers
INSERT INTO product_catalog_product_children (parent_product_dbid, child_product_dbid, count) (
    SELECT parent.dbid, child.dbid, 1 FROM product_catalog_product parent
        JOIN product_catalog_product child
        ON parent.product_id = '450ed1f4-6bd8-4086-8e85-737584945fef' AND child.product_id IN ('6ad0b1e1-9a47-4f8b-92ba-6d9ed88c92f5', 'f11b5b99-44cc-43bf-a12d-0b64fb8aae8a')
);

INSERT INTO product_catalog_product_localized (product_dbid, de, de_de) (
    SELECT dbid, '{"active": true}', '{"displayName": "Zahnschienen Standard with Retainer"}'
    FROM product_catalog_product WHERE name = 'aligners-with-retainers'
);

-- pro-aligners-with-retainers
INSERT INTO product_catalog_product_children (parent_product_dbid, child_product_dbid, count) (
    SELECT parent.dbid, child.dbid, 1 FROM product_catalog_product parent
        JOIN product_catalog_product child
        ON parent.product_id = '6b2ee5ad-c9d3-4de1-ae54-5279e4561d8a' AND child.product_id IN ('6d940e50-f8e1-4ff9-9f9f-6a2bc8d09279', 'f11b5b99-44cc-43bf-a12d-0b64fb8aae8a')
);

INSERT INTO product_catalog_product_localized (product_dbid, de, de_de) (
    SELECT dbid, '{"active": true}', '{"displayName": "Zahnschienen Pro with Retainer"}'
    FROM product_catalog_product WHERE name = 'pro-aligners-with-retainers'
);
