<?php

declare(strict_types=1);

namespace Vijoni\Tech\Heartbeat;

use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
}
