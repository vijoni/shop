<?php

declare(strict_types=1);

namespace Vijoni\Application\DependencyProvider;

use Vijoni\Database\Client\DatabaseClient as OwnDatabaseClient;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
trait DatabaseClient
{
  /**
   * @return OwnDatabaseClient
   */
  public function shareDatabaseClient(): OwnDatabaseClient
  {
    /** @var OwnDatabaseClient */
    return $this->dependencyProvider()->share(DependencyProviderKeys::MAIN_DATABASE);
  }
}
