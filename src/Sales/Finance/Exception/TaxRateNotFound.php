<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Repository;

class TaxRateNotFound extends \Exception
{
}
