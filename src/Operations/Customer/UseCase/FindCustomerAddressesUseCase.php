<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\UseCase;

use Vijoni\Operations\Customer\Repository\CustomerReadRepository;
use Vijoni\Operations\Shared\Address;

class FindCustomerAddressesUseCase
{
  public function __construct(private CustomerReadRepository $customerRepository)
  {
  }

  /**
   * @return Address[]
   */
  public function findCustomerAddressesByCustomerId(string $customerId): array
  {
    $addresses = $this->customerRepository->findCustomerAddressesByCustomerId($customerId);
    $indexedAddresses = [];
    foreach ($addresses as $address) {
      $indexedAddresses[$address->getAddressId()] = $address;
    }

    return $indexedAddresses;
  }
}
