<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Repository;

use Vijoni\Operations\Shared\Customer;
use Vijoni\Operations\Shared\CustomerDbTable;

class CustomerMapper
{
  public function mapDbRowToCustomer(array $row): Customer
  {
    $customer = new Customer();
    $customer->fromArray([
      Customer::CUSTOMER_ID => $row[CustomerDbTable::CUSTOMER_ID],
      Customer::FIRSTNAME => $row[CustomerDbTable::FIRSTNAME],
      Customer::LASTNAME => $row[CustomerDbTable::LASTNAME],
      Customer::EMAIL => $row[CustomerDbTable::EMAIL],
      Customer::BIRTH_DATE => new \DateTime($row[CustomerDbTable::BIRTH_DATE]),
    ]);

    return $customer;
  }

  public function mapCustomerToDbRow(Customer $customer): array
  {
    $dbValues = $customer->mapModifiedTo([
      Customer::FIRSTNAME => CustomerDbTable::FIRSTNAME,
      Customer::LASTNAME => CustomerDbTable::LASTNAME,
      Customer::EMAIL => CustomerDbTable::EMAIL,
      Customer::BIRTH_DATE => CustomerDbTable::BIRTH_DATE,
    ]);

    if (isset($dbValues[CustomerDbTable::BIRTH_DATE])) {
      $dbValues[CustomerDbTable::BIRTH_DATE] = $customer->getBirthDate()->format('Y-m-d H:i:s');
    }

    return $dbValues;
  }
}
