<?php

declare(strict_types=1);

namespace Vijoni\Application\SchemaValidator;

use JsonException;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Errors\ValidationError;
use Opis\JsonSchema\Validator;

class JsonSchemaValidator
{
  private Validator $validator;

  private static ErrorFormatter $errorFormatter;

  public function __construct()
  {
    $this->validator = new Validator();
    $this->validator->setMaxErrors(PHP_INT_MAX);

    if (!isset(self::$errorFormatter)) {
      self::$errorFormatter = new ErrorFormatter();
    }
  }

  /**
   * @throws JsonException
   */
  public function validate(string $payload, string $schemaFilePath, array $messageTemplatesMap = []): array
  {
    $data = json_decode($payload, false, 512, JSON_THROW_ON_ERROR);
    $schema = file_get_contents($schemaFilePath);
    $result = $this->validator->validate($data, $schema);

    if ($result->error() === null) {
      return [];
    }

    $messageBuilder = fn (ValidationError $error, string $message)
      => $this->formatErrors($error, $message, $messageTemplatesMap);

    return self::$errorFormatter->format($result->error(), true, $messageBuilder);
  }

  private function formatErrors(
    ValidationError $error,
    string $rawMessage,
    array $messageMap = []
  ): array {
    $formattedMessage = self::$errorFormatter->formatErrorMessage($error);
    $outputMessage = $messageMap[$rawMessage] ?? $formattedMessage;
    [$errorCode, $errorField] = $this->parseRawMessage($rawMessage);

    return [
      'message' => self::$errorFormatter->formatErrorMessage($error, $outputMessage),
      'error' => $rawMessage,
      'errorCode' => $errorCode,
      'errorField' => $errorField,
      'fullPath' => $error->data()->fullPath(),
      'tech' => [
        'errorKeyword' => $error->keyword(),
        'errorArgs' => $error->args(),
        'messageTemplate' => $error->message(),
        'formattedMessage' => $formattedMessage,
      ],
    ];
  }

  private function parseRawMessage(string $rawMessage): array
  {
    $messageParts = explode('::', $rawMessage);
    return [
      $messageParts[0] ?? '',
      $messageParts[1] ?? '',
    ];
  }
}
