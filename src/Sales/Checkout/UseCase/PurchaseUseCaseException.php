<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\UseCase;

use Vijoni\Sales\Shared\Result\PurchaseUseCaseResult;

class PurchaseUseCaseException extends \Exception
{
  public function __construct(private PurchaseUseCaseResult $result)
  {
    $errorCode = $result->getFirstError()->getErrorCode();
    parent::__construct("Purchase exception: {$errorCode}");
  }

  public function getUseCaseResult(): PurchaseUseCaseResult
  {
    return $this->result;
  }
}
