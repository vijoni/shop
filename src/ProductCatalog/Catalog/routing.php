<?php

declare(strict_types=1);

use Slim\SlimApp;
use Vijoni\ProductCatalog\Catalog\Http\FindProductAction;

return function (SlimApp $slimApp): void {
  $slimApp->get('/product/{productId}/{countryCode}/{locale}', FindProductAction::class)
    ->setName('get-product');
};
