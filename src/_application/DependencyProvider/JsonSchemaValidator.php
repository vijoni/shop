<?php

declare(strict_types=1);

namespace Vijoni\Application\DependencyProvider;

use Vijoni\Application\SchemaValidator\JsonSchemaValidator as OwnJsonSchemaValidator;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
trait JsonSchemaValidator
{
  /**
   * @return OwnJsonSchemaValidator
   */
  public function shareJsonSchemaValidator(): OwnJsonSchemaValidator
  {
    /** @var OwnJsonSchemaValidator */
    return $this->dependencyProvider()->share(DependencyProviderKeys::JSON_SCHEMA_VALIDATOR);
  }
}
