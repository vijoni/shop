<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order\Repository;

class MapperFactory
{
  public function newOrderMapper(): OrderMapper
  {
    return new OrderMapper();
  }
}
