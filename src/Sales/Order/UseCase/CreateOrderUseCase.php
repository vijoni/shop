<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order\UseCase;

use Vijoni\Sales\Order\Repository\OrderWriteRepository;
use Vijoni\Sales\Shared\Order;

class CreateOrderUseCase
{
  public function __construct(
    private OrderWriteRepository $orderWriteRepository
  ) {
  }

  public function createOrder(Order $order): bool
  {
    $this->orderWriteRepository->createOrder($order);

    return true;
  }
}
