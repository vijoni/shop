<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared;

class ProductChildCollection extends \ArrayIterator
{
  /**
   * @param ProductChild[] $productChildren
   * @param int $flags
   */
  public function __construct(array $productChildren = [], int $flags = 0)
  {
    parent::__construct($productChildren, $flags);
  }

  /**
   * @param ProductChild $value
   * @return void
   */
  public function append(mixed $value): void
  {
    parent::append($value);
  }

  /**
   * @return ProductChild
   */
  public function current(): mixed
  {
    /** @var ProductChild */
    return parent::current();
  }

  /**
   * @param string $key
   * @return ProductChild
   */
  public function offsetGet(mixed $key): mixed
  {
    /** @var ProductChild */
    return parent::offsetGet($key);
  }

  /**
   * @param string $key
   * @param ProductChild $value
   * @return void
   */
  public function offsetSet(mixed $key, mixed $value): void
  {
    parent::offsetSet($key, $value);
  }

  /**
   * @param string $key
   * @return void
   */
  public function offsetUnset(mixed $key): void
  {
    parent::offsetUnset($key);
  }

  public function toDeepArray(): array
  {
    $asArray = [];
    foreach ($this as $productId => $productChild) {
      $asArray[$productId] = $productChild->toDeepArray();
    }

    return $asArray;
  }

  /**
   * @return string[]
   */
  public function allProductIds(): array
  {
    return array_keys(iterator_to_array($this));
  }
}
