<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedTotals;

class Totals extends GeneratedTotals
{
  public function addPriceToPay(string $priceToPay): void
  {
    $this->setPriceToPay(bcadd($this->getPriceToPay(), $priceToPay));
  }

  public function addNetPrice(string $netPrice): void
  {
    $this->setNetPrice(bcadd($this->getNetPrice(), $netPrice));
  }

  public function addTaxValue(string $taxValue): void
  {
    $this->setTaxValue(bcadd($this->getTaxValue(), $taxValue));
  }

  public function addDiscountValue(string $discountValue): void
  {
    $this->setDiscountValue(bcadd($this->getDiscountValue(), $discountValue));
  }
}
