-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE product_catalog_product_children(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    parent_product_dbid UUID NOT NULL REFERENCES product_catalog_product(dbid),
    child_product_dbid UUID NOT NULL REFERENCES product_catalog_product(dbid),
    count INT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:002
CREATE TRIGGER update_product_catalog_product_children_updated_at
    BEFORE UPDATE ON product_catalog_product_children
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
