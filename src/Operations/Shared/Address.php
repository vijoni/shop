<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared;

use Vijoni\Operations\Shared\Generated\GeneratedAddress;

class Address extends GeneratedAddress
{
}
