<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Service;

use Vijoni\ProductCatalog\UnitFacade as ProductCatalogFacade;
use Vijoni\Sales\Finance\ModuleFacade as FinanceFacade;
use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\ProductChild;
use Vijoni\Sales\Shared\Region;
use Vijoni\Sales\Shared\TaxRate;

class CheckoutProductReader
{
  public function __construct(
    private ProductCatalogFacade $productCatalogFacade,
    private FinanceFacade $financeFacade
  ) {
  }

  /**
   * @param OrderItem[] $orderItems
   * @return Product[]
   */
  public function readProducts(array $orderItems, Region $region): array
  {
    $productIds = array_map(static fn(OrderItem $orderItem) => $orderItem->getProductId(), $orderItems);
    $productsAttributes = $this->productCatalogFacade->findProductsByIdByRegion(
      $productIds,
      $region->getCountryCode(),
      $region->getLocale()
    );
    $taxRates = $this->financeFacade->findTaxRatesByCountry($region->getCountryCode());

    return $this->createProducts($productsAttributes, $orderItems, $taxRates);
  }

  /**
   * @param OrderItem[] $orderItems
   * @param Product[] $products
   * @return void
   */
  public function mergeOrderItemsPropertiesIntoProducts(array $orderItems, array $products)
  {
    foreach ($orderItems as $orderItem) {
      $product = $products[$orderItem->getProductId()];
      $product->setPriceToPay($orderItem->getPrice());
      $this->applyProductCount($orderItem, $product);
      $this->financeFacade->calculateProductTaxes($product);
    }
  }

  /**
   * @param OrderItem[] $orderItems
   * @param TaxRate[] $taxRates
   * @return Product[]
   */
  private function createProducts(array $productsAttributes, array $orderItems, array $taxRates): array
  {
    $products = [];
    foreach ($orderItems as $orderItem) {
      $productAttributes = $productsAttributes[$orderItem->getProductId()];
      $product = $this->newProductFromAttributes($productAttributes, $taxRates);
      $products[$product->getProductId()] = $product;
    }

    return $products;
  }

  /**
   * @param TaxRate[] $taxRates
   */
  private function newProductFromAttributes(array $attributes, array $taxRates): Product
  {
    $product = new Product();
    $product->intersect(
      [
        'productId' => Product::PRODUCT_ID,
        'name' => Product::NAME,
        'type' => Product::TYPE,
        'isActive' => Product::IS_ACTIVE,
        'displayName' => Product::DISPLAY_NAME,
        'price' => Product::BASE_PRICE,
        'count' => Product::COUNT,
      ],
      $attributes
    );
    $product->setPriceToPay($product->getBasePrice());

    $this->createChildren($product, $attributes);
    $this->createChildProducts($product, $attributes, $taxRates);
    $this->sumChildSetPrices($product);
    $this->calculateProductTaxes($product, $attributes, $taxRates);

    return $product;
  }

  /**
   * @param Product $product
   * @param TaxRate[] $taxRates
   * @return void
   */
  private function applyTaxRate(Product $product, string $taxRateType, array $taxRates): void
  {
    $taxRate = $taxRates[$taxRateType]->getValue();
    $product->setTaxRate(rawTaxRateToPercentageTaxRate($taxRate));
  }

  private function createChildren(Product $product, array $attributes): void
  {
    if (empty($attributes['children'])) {
      return;
    }

    foreach ($attributes['children'] as $childAttributes) {
      $child = new ProductChild();
      $child->setProductId($childAttributes['productId']);
      $child->setCount($childAttributes['count']);
      $product->addChild($child);
    }
  }

  private function createChildProducts(Product $product, array $attributes, array $taxRates): void
  {
    if (!$product->hasChildren()) {
      return;
    }

    foreach ($attributes['childProducts'] as $productAttributes) {
      $childProduct = $this->newProductFromAttributes($productAttributes, $taxRates);
      $childProduct->setParentProductId($product->getProductId());
      $product->addChildProduct($childProduct);
    }
  }

  private function sumChildSetPrices(Product $product): void
  {
    if (!$product->isTypeSet()) {
      return;
    }

    $sumPrice = '';
    foreach ($product->getChildProducts() as $childProduct) {
      $count = (string)$childProduct->getCount();
      $totalPrice = bcmul($childProduct->getPriceToPay(), $count);
      $sumPrice = bcadd($sumPrice, $totalPrice);
    }

    $product->setPriceToPay(roundCents($sumPrice));
  }

  /**
   * @param TaxRate[] $taxRates
   */
  private function calculateProductTaxes(Product $product, array $attributes, array $taxRates): void
  {
    if (!$product->isTypeSet()) {
      $this->applyTaxRate($product, $attributes['taxRateType'], $taxRates);
      $this->financeFacade->calculateProductTaxes($product);
    }
  }

  private function applyProductCount(OrderItem $orderItem, Product $product): void
  {
    $product->setOrderedCount($orderItem->getCount());

    foreach ($product->getChildProducts() as $childProduct) {
      $childProduct->setOrderedCount($childProduct->getCount() * $product->getOrderedCount());
    }
  }
}
