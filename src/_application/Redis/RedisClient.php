<?php

declare(strict_types=1);

namespace Vijoni\Application\Redis;

use Redis;

class RedisClient
{
  private Redis $redis;

  public function __construct(
    private string $host,
    private int $port,
    private int $databaseIndex,
  ) {
    $this->redis = new Redis();
  }

  public function init(): void
  {
    $this->redis->connect($this->host, $this->port);
    $this->redis->select($this->databaseIndex);
  }

  public function getString(string $key): ?string
  {
    $value = $this->redis->get($key);
    if ($value === false) {
      return null;
    }

    return $value;
  }

  public function getInt(string $key): ?int
  {
    $value = $this->redis->get($key);
    if ($value === false) {
      return null;
    }

    $value = filter_var($value, FILTER_VALIDATE_INT);
    if ($value === false) {
      throw new RedisException("Value could not be converted to integer: [{$key}]");
    }

    return $value;
  }

  public function setEx(string $key, int $ttl, string $value): bool
  {
    return $this->redis->setEx($key, $ttl, $value);
  }

  public function setNxEx(string $key, int $ttlSeconds, string $value): bool
  {
    return $this->redis->set($key, $value, ['nx', 'ex' => $ttlSeconds]);
  }

  public function unlink(string $key): int
  {
    return $this->redis->unlink($key);
  }
}
