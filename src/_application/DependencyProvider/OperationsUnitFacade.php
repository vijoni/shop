<?php

declare(strict_types=1);

namespace Vijoni\Application\DependencyProvider;

use Vijoni\Operations\UnitFacade as OperationsFacade;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
trait OperationsUnitFacade
{
  /**
   * @return OperationsFacade
   */
  public function shareOperationsUnitFacade(): OperationsFacade
  {
    /** @var OperationsFacade */
    return $this->dependencyProvider()->shareUnitFacade(OperationsFacade::class);
  }
}
