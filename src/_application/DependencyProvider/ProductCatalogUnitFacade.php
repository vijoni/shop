<?php

declare(strict_types=1);

namespace Vijoni\Application\DependencyProvider;

use Vijoni\ProductCatalog\UnitFacade as ProductCatalogFacade;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
trait ProductCatalogUnitFacade
{
  /**
   * @return ProductCatalogFacade
   */
  public function shareProductCatalogUnitFacade(): ProductCatalogFacade
  {
    /** @var ProductCatalogFacade */
    return $this->dependencyProvider()->shareUnitFacade(ProductCatalogFacade::class);
  }
}
