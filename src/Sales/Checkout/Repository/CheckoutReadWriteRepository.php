<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Repository;

class CheckoutReadWriteRepository
{
  public function __construct(
    private CheckoutReadRepository $cartReadRepository,
    private CheckoutWriteRepository $cartWriteRepository
  ) {
  }

  public function read(): CheckoutReadRepository
  {
    return $this->cartReadRepository;
  }

  public function write(): CheckoutWriteRepository
  {
    return $this->cartWriteRepository;
  }
}
