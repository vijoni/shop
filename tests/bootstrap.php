<?php

declare(strict_types=1);

use Vijoni\Application;
use Vijoni\Application\DependencyProvider\DependencyProviderKeys;
use Vijoni\Database\Client\PgDatabaseClient;
use Vijoni\Unit\AppConfig;
use Vijoni\Unit\DependencyProvider;
use VijoniTest\Database\DbInit;

require_once __DIR__ . '/../src/bootstrap.php';
require_once __DIR__ . '/../src/internal_functions.php';
require_once __DIR__ . '/../tools/LiquibaseCommand.php';

function overwriteDatabaseConfig(array $configProperties): array
{
  $dbConfig = &$configProperties['database'];

  $dbInit = new DbInit();
  $testDbName = $dbInit->createTestDatabase($dbConfig['user']);

  $dbConfig['database'] = $testDbName;

  $dbUrl = buildDbUrl(
    $dbConfig['host'],
    $dbConfig['port'],
    $dbConfig['database'],
    $dbConfig['user'],
    $dbConfig['password']
  );

  $dbConfig['master_url'] = $dbUrl;
  $dbConfig['slave_url'] = $dbUrl;

  return $configProperties;
}

function registerDatabaseClient(AppConfig $appConfig): void
{
  $dbConfig = $appConfig->extractConfig('database');

  $dbClient = new PgDatabaseClient($dbConfig->getString('master_url'), $dbConfig->getString('slave_url'));

  DependencyProvider::getInstance()->register(
    DependencyProviderKeys::MAIN_DATABASE,
    static function () use ($dbClient) {
      $dbClient->init();

      return $dbClient;
    }
  );
}

$configProperties = require(__DIR__ . '/../config/_generated/config.php');
$configProperties = overwriteDatabaseConfig($configProperties);

$appConfig = new AppConfig($configProperties);

$cmd = new LiquibaseCommand($appConfig);
$cmd->init();
$cmd->run('update');

$app = new Application($appConfig);

registerDatabaseClient($appConfig);

$app->initDependencyProvider();
