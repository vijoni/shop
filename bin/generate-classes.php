#!/usr/bin/env php

<?php

use Vijoni\ClassGenerator\SchemaFinder;
use Vijoni\ClassGenerator\Generator;

require_once $_composer_autoload_path ?? __DIR__ . '/../vendor/autoload.php';

$srcDirectory = __DIR__ . '/../src';

$classGenerator = new Generator($srcDirectory, new SchemaFinder());
$unresolvedDependencies = $classGenerator->generate('class-schema-*.yml');

if (!empty($unresolvedDependencies)) {
  var_dump($unresolvedDependencies);
  exit(1);
}

exit(0);
