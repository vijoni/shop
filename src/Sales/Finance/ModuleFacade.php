<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance;

use Vijoni\Sales\Shared\Payment;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\TaxRate;
use Vijoni\Sales\Shared\Totals;
use Vijoni\Unit\BaseModuleFacade;

/**
 * @method ModuleFactory moduleFactory()
 */
class ModuleFacade extends BaseModuleFacade
{
  /**
   * @return TaxRate[]
   */
  public function findTaxRatesByCountry(string $countryCode): array
  {
    return $this->moduleFactory()->shareFinanceReadRepository()->findTaxRatesByCountry($countryCode);
  }

  public function calculateProductTaxes(Product $product): void
  {
    $this->moduleFactory()->shareSalesCalculator()->calculateProductTaxes($product);
  }

  /**
   * @param Product[] $products
   */
  public function calculateAllProductSums(array $products): void
  {
    $this->moduleFactory()->shareSalesCalculator()->calculateAllProductSums($products);
  }

  /**
   * @param Product[] $products
   * @return Totals[]
   */
  public function calculateTotalsPerTaxRate(array $products): array
  {
    return $this->moduleFactory()->shareSalesCalculator()->calculateTotalsPerTaxRate($products);
  }

  /**
   * @param Totals[] $taxRateTotals
   * @return Totals
   */
  public function calculateTotalsFromTaxRateTotals(array $taxRateTotals): Totals
  {
    return $this->moduleFactory()->shareSalesCalculator()->calculateTotalsFromTaxRateTotals($taxRateTotals);
  }

  /**
   * @param string[] $paymentProviders
   * @return Payment[]
   */
  public function createPayments(array $paymentProviders): array
  {
    return $this->moduleFactory()->newPaymentFactory()->createPayments($paymentProviders);
  }
}
