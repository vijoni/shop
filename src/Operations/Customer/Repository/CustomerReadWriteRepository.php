<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Repository;

class CustomerReadWriteRepository
{
  public function __construct(
    private CustomerReadRepository $customerReadRepository,
    private CustomerWriteRepository $customerWriteRepository
  ) {
  }

  public function read(): CustomerReadRepository
  {
    return $this->customerReadRepository;
  }

  public function write(): CustomerWriteRepository
  {
    return $this->customerWriteRepository;
  }
}
