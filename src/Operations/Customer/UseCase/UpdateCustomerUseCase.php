<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\UseCase;

use Vijoni\Application\Error;
use Vijoni\Application\Result;
use Vijoni\Operations\Customer\Repository\CustomerReadWriteRepository;
use Vijoni\Operations\Shared\Customer;

class UpdateCustomerUseCase
{
  public const ERROR_NOT_FOUND = 'ERROR_NOT_FOUND';

  public function __construct(private CustomerReadWriteRepository $customerRepository)
  {
  }

  public function updateCustomer(Customer $customer): Result
  {
    $id = $customer->getCustomerId();
    $existingCustomer = $this->customerRepository->read()->findCustomerById($id);
    if (!$existingCustomer->hasId()) {
      return new Result(false, [new Error(self::ERROR_NOT_FOUND, ['customerId' => $customer->getCustomerId()])]);
    }

    $this->customerRepository->write()->updateCustomer($customer);

    return new Result(true);
  }
}
