<?php

declare(strict_types=1);

namespace Vijoni\Application\Http;

use Slim\Factory\AppFactory;
use Slim\SlimApp;

class Router
{
  private const APP_DIR = __DIR__ . '/../../..';

  public function initAndDispatch(): void
  {
    $slimApp = AppFactory::create();

    $this->loadRoutes($slimApp);

    $slimApp->addErrorMiddleware(true, true, true);

    $slimApp->run();
  }

  private function loadRoutes(SlimApp $slimApp): void
  {
    $routeCollector = $slimApp->getRouteCollector();
    $routeCollector->setCacheFile(self::APP_DIR . '/cache/router/routes');

    $filePaths = $this->readRoutingFilePaths();
    foreach ($filePaths as $filePath) {
      $routesCallback = require_once($filePath);
      $routesCallback($slimApp);
    }
  }

  private function readRoutingFilePaths(): array
  {
    $cacheFile = self::APP_DIR . '/cache/router/routing_files.php';
    if (file_exists($cacheFile)) {
      return require($cacheFile);
    }

    return $this->createRoutingFilePathsCacheFile($cacheFile);
  }

  private function createRoutingFilePathsCacheFile(string $cacheFilePath): array
  {
    $filePaths = glob(self::APP_DIR . '/src/*/*/routing.php');
    if ($filePaths === false) {
      throw new \Exception('Found no routing files');
    }

    $phpCode =  var_export($filePaths, true);
    $cacheContent = <<<PHP
<?php

return {$phpCode};

PHP;

    file_put_contents($cacheFilePath, $cacheContent);

    return $filePaths;
  }
}
