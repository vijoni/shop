<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Service;

class CheckoutReaderAggregate
{
  public function __construct(
    private CheckoutProductReader $productReader,
    private CheckoutPaymentReader $paymentReader,
    private CheckoutAddressReader $addressReader
  ) {
  }

  public function getProductReader(): CheckoutProductReader
  {
    return $this->productReader;
  }

  public function getPaymentReader(): CheckoutPaymentReader
  {
    return $this->paymentReader;
  }

  public function getAddressReader(): CheckoutAddressReader
  {
    return $this->addressReader;
  }
}
