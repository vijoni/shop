<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance;

use Vijoni\Application\DependencyProvider\DatabaseClient;
use Vijoni\Application\DependencyProvider\JsonSchemaValidator;
use Vijoni\Unit\BaseModuleDependencyProvider;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
  use DatabaseClient;
}
