<?php

declare(strict_types=1);

namespace VijoniTest\Helper\Operations;

use Faker\Factory as FakerFactory;
use Vijoni\Operations\Shared\Customer;

class CustomerBuilder
{
  public static function buildCustomer(): Customer
  {
    $faker = FakerFactory::create();

    $customer = new Customer();
    $customer->setFirstname($faker->firstName());
    $customer->setLastname($faker->lastName());
    $customer->setEmail($faker->email());
    $customer->setBirthDate($faker->dateTime());

    return $customer;
  }
}
