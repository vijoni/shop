<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedAddress;

class Address extends GeneratedAddress
{
}
