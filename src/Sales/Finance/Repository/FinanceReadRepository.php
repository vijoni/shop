<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Repository;

use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Sales\Shared\TaxRate;
use Vijoni\Sales\Shared\TaxRateDbTable;

class FinanceReadRepository
{
  public function __construct(private DatabaseClient $db, private MapperFactory $mapperFactory)
  {
  }

  /**
   * @return TaxRate[]
   */
  public function findTaxRatesByCountry(string $countryCode): array
  {
    $qb = $this->db->newQueryBuilder(
      'SELECT * FROM sales_tax_rate WHERE country_code = %s',
      [$countryCode],
      [],
      "sales: find tax rate by country code; countryCode:[{$countryCode}]"
    );
    $result = $this->db->queryAll($qb);

    if (empty($result)) {
      throw new TaxRateNotFound("For country [{$countryCode}]");
    }

    $taxRateMapper = $this->mapperFactory->newTaxRateMapper();

    return $taxRateMapper->mapDbRowsToTaxRates($result);
  }
}
