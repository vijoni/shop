-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE operations_customer(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    customer_id UUID NOT NULL UNIQUE DEFAULT uuid_generate_v1mc(),
    firstname VARCHAR NOT NULL,
    lastname VARCHAR NOT NULL,
    email VARCHAR NOT NULL UNIQUE,
    birth_date TIMESTAMP WITH TIME ZONE NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changeset kgawlinski:002
CREATE INDEX IF NOT EXISTS operations_customer__customer_id ON operations_customer USING BTREE (customer_id);
CREATE INDEX IF NOT EXISTS operations_customer__email ON operations_customer USING BTREE (email);

-- changset kgawlinski:003
CREATE TRIGGER update_operations_customer_updated_at
    BEFORE UPDATE ON operations_customer
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
