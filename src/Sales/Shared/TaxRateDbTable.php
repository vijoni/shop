<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class TaxRateDbTable
{
  public const COUNTRY_CODE = 'country_code';
  public const TYPE = 'type';
  public const VALUE = 'value';
}
