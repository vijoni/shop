<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

interface InstallmentPayment
{
  public function calculateAllVariants(Totals $totals): array;

  public function calculateVariant(Totals $totals, string $variant): Totals;
}
