<?php

declare(strict_types=1);

namespace Vijoni\Tech\Heartbeat\Http;

use Nyholm\Psr7\Response;
use Vijoni\Tech\Heartbeat\ModuleFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class HeartbeatAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    return new Response(200);
  }
}
