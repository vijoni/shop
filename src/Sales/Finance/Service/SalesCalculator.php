<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Service;

use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\Totals;

class SalesCalculator
{
  public function calculateProductTaxes(Product $product): void
  {
    if ($product->isTypeSet()) {
      $this->calculateProductSetTaxes($product);
    } else {
      $taxValue = $this->taxValueFromGrossPrice($product->getPriceToPay(), $product->getTaxRate());
      $netPrice = $this->netPrice($product->getPriceToPay(), $product->getTaxRate());
      $product->setTaxValue($taxValue);
      $product->setNetPrice($netPrice);
    }

    $product->setTaxValue(roundCents($product->getTaxValue()));
    $product->setNetPrice(roundCents($product->getNetPrice()));
  }

  /**
   * @param Product[] $products
   */
  public function calculateAllProductSums(array $products): void
  {
    foreach ($products as $product) {
      $this->calculateProductSums($product);
    }
  }

  private function calculateProductSetTaxes(Product $product): void
  {
    foreach ($product->getChildProducts() as $childProduct) {
      $product->setTaxValue(bcadd($product->getTaxValue(), $childProduct->getTaxValue()));
      $product->setNetPrice(bcadd($product->getNetPrice(), $childProduct->getNetPrice()));
    }
  }

  private function calculateProductSums(Product $product): void
  {
    $this->calculateSetChildProductsSums($product);

    $count = (string)$product->getOrderedCount();

    $product->setTotalPriceToPay(roundCents(bcmul($product->getPriceToPay(), $count)));
    $product->setTotalNetPrice(roundCents(bcmul($product->getNetPrice(), $count)));
    $product->setTotalTaxValue(roundCents(bcmul($product->getTaxValue(), $count)));
  }

  private function calculateSetChildProductsSums(Product $product): void
  {
    if ($product->isTypeSet()) {
      foreach ($product->getChildProducts() as $childProduct) {
        $this->calculateProductSums($childProduct);
      }
    }
  }

  /**
   * @param Totals[] $taxRateTotals
   */
  public function calculateTotalsFromTaxRateTotals(array $taxRateTotals): Totals
  {
    $resultTotals = new Totals();
    foreach ($taxRateTotals as $totals) {
      $resultTotals->addPriceToPay($totals->getPriceToPay());
      $resultTotals->addNetPrice($totals->getNetPrice());
      $resultTotals->addTaxValue($totals->getTaxValue());
    }

    $resultTotals->setPriceToPay(roundCents($resultTotals->getPriceToPay()));
    $resultTotals->setNetPrice(roundCents($resultTotals->getNetPrice()));
    $resultTotals->setTaxValue(roundCents($resultTotals->getTaxValue()));

    return $resultTotals;
  }

  /**
   * @param Product[] $products
   * @return Totals[]
   */
  public function calculateTotalsPerTaxRate(array $products): array
  {
    $result = [];
    $this->deepCalculateTotalsPerTaxRate($products, $result);

    foreach ($result as $totals) {
      $this->roundTotals($totals);
    }

    return $result;
  }

  /**
   * @param Product[] $products
   * @return Totals[]
   */
  private function deepCalculateTotalsPerTaxRate(array $products, array &$taxRateTotals = []): array
  {
    foreach ($products as $product) {
      if ($product->isTypeSet()) {
        $this->deepCalculateTotalsPerTaxRate($product->getChildProducts(), $taxRateTotals);
      } else {
        $taxRate = $product->getTaxRate();
        $totals = $taxRateTotals[$taxRate] ?: new Totals();

        $taxRateTotals[$taxRate] = $totals;
        $totals->addPriceToPay($product->getTotalPriceToPay());
        $totals->addNetPrice($product->getTotalNetPrice());
        $totals->addTaxValue($product->getTotalTaxValue());
      }
    }

    return $taxRateTotals;
  }

  private function taxValueFromGrossPrice(string $grossPrice, string $taxRate): string
  {
    //example for 120.00EUR and 19%
    // (19% + 100) / 100
    $taxIncludedFactor = bcdiv(bcadd($taxRate, '100'), '100'); // for 19% = 1.19
    // 19% / 100
    $taxFactor = bcdiv($taxRate, '100'); // for 19% = 0.19

    // 120.00 / 1.19 * 0.19
    return bcmul(bcdiv($grossPrice, $taxIncludedFactor), $taxFactor);
  }

  private function netPrice(string $grossPrice, string $taxRate): string
  {
    //example for 120.00EUR and 19%
    // (19% + 100) / 100
    $taxIncludedFactor = bcdiv(bcadd($taxRate, '100'), '100'); // for 19% = 1.19

    // 120.00 / 1.19
    return bcdiv($grossPrice, $taxIncludedFactor);
  }

  private function roundTotals(Totals $totals): void
  {
    $totals->setPriceToPay(roundCents($totals->getPriceToPay()));
    $totals->setNetPrice(roundCents($totals->getNetPrice()));
    $totals->setTaxValue(roundCents($totals->getTaxValue()));
  }
}
