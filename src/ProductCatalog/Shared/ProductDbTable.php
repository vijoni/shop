<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared;

class ProductDbTable
{
  public const PRODUCT_ID = 'product_id';
  public const NAME = 'name';
  public const TYPE = 'type';

  public const ALIAS_ATTRIBUTES = 'attributes';
  public const ALIAS_CHILDREN_ID = 'children_id';
  public const ALIAS_CHILDREN_COUNT = 'children_count';
}
