sudo -u postgres psql -tc "SELECT datname, pg_terminate_backend(pid) FROM pg_stat_activity WHERE state='idle' AND state_change < NOW() - INTERVAL '10 minutes';"

sudo -u postgres psql -tc "SELECT format('DROP DATABASE %s;', db.datname) FROM pg_stat_activity stat RIGHT JOIN pg_database db ON stat.datname = db.datname WHERE stat.datname IS NULL AND db.datname LIKE 'database_shop_test_%' GROUP BY db.datname;" \
| grep "\S" \
| xargs -t -d "\n" -I % sudo -u postgres psql -c "%"

