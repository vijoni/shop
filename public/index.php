<?php

use Vijoni\Application;
use Vijoni\Unit\AppConfig;

require_once __DIR__ . '/../src/bootstrap.php';

$appConfig = AppConfig::fromFile(__DIR__ . '/../config/_generated/config.php');
$app = new Application($appConfig);
$app->initDependencyProvider();
$app->dispatch();
