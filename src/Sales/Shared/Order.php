<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedOrder;

class Order extends GeneratedOrder
{
  public const STATUS_WAITING_FOR_PAYMENT = 'waiting_for_payment';
}
