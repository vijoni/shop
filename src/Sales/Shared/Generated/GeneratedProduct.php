<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedProduct extends ClassBase
{
  public const TYPE_SIMPLE = 'simple';
  public const TYPE_SET = 'set';

  private string $productId = '';
  private string $parentProductId = '';
  private bool $isActive =  false;
  private string $name = '';
  private string $type = '';
  private string $basePrice = '';
  private string $priceToPay = '';
  private string $totalPriceToPay = '';
  private string $netPrice = '';
  private string $totalNetPrice = '';
  private string $taxRate = '';
  private string $taxValue = '';
  private string $totalTaxValue = '';
  private string $discountValue =  '0';
  private string $totalDiscount =  '0';
  private string $displayName = '';
  private array $children = [];
  private array $childProducts = [];
  private int $count = -1;
  private int $orderedCount = -1;

  public const PRODUCT_ID = 'productId';
  public const PARENT_PRODUCT_ID = 'parentProductId';
  public const IS_ACTIVE = 'isActive';
  public const NAME = 'name';
  public const TYPE = 'type';
  public const BASE_PRICE = 'basePrice';
  public const PRICE_TO_PAY = 'priceToPay';
  public const TOTAL_PRICE_TO_PAY = 'totalPriceToPay';
  public const NET_PRICE = 'netPrice';
  public const TOTAL_NET_PRICE = 'totalNetPrice';
  public const TAX_RATE = 'taxRate';
  public const TAX_VALUE = 'taxValue';
  public const TOTAL_TAX_VALUE = 'totalTaxValue';
  public const DISCOUNT_VALUE = 'discountValue';
  public const TOTAL_DISCOUNT = 'totalDiscount';
  public const DISPLAY_NAME = 'displayName';
  public const CHILDREN = 'children';
  public const CHILD_PRODUCTS = 'childProducts';
  public const COUNT = 'count';
  public const ORDERED_COUNT = 'orderedCount';

  public function getProductId(): string
  {
    return $this->productId;
  }

  public function setProductId(string $productId): void
  {
    $this->productId = $productId;
    $this->modified[self::PRODUCT_ID] = true;
  }

  public function getParentProductId(): string
  {
    return $this->parentProductId;
  }

  public function setParentProductId(string $parentProductId): void
  {
    $this->parentProductId = $parentProductId;
    $this->modified[self::PARENT_PRODUCT_ID] = true;
  }

  public function getIsActive(): bool
  {
    return $this->isActive;
  }

  public function setIsActive(bool $isActive): void
  {
    $this->isActive = $isActive;
    $this->modified[self::IS_ACTIVE] = true;
  }

  public function getName(): string
  {
    return $this->name;
  }

  public function setName(string $name): void
  {
    $this->name = $name;
    $this->modified[self::NAME] = true;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): void
  {
    $this->type = $type;
    $this->modified[self::TYPE] = true;
  }

  public function getBasePrice(): string
  {
    return $this->basePrice;
  }

  public function setBasePrice(string $basePrice): void
  {
    $this->basePrice = $basePrice;
    $this->modified[self::BASE_PRICE] = true;
  }

  public function getPriceToPay(): string
  {
    return $this->priceToPay;
  }

  public function setPriceToPay(string $priceToPay): void
  {
    $this->priceToPay = $priceToPay;
    $this->modified[self::PRICE_TO_PAY] = true;
  }

  public function getTotalPriceToPay(): string
  {
    return $this->totalPriceToPay;
  }

  public function setTotalPriceToPay(string $totalPriceToPay): void
  {
    $this->totalPriceToPay = $totalPriceToPay;
    $this->modified[self::TOTAL_PRICE_TO_PAY] = true;
  }

  public function getNetPrice(): string
  {
    return $this->netPrice;
  }

  public function setNetPrice(string $netPrice): void
  {
    $this->netPrice = $netPrice;
    $this->modified[self::NET_PRICE] = true;
  }

  public function getTotalNetPrice(): string
  {
    return $this->totalNetPrice;
  }

  public function setTotalNetPrice(string $totalNetPrice): void
  {
    $this->totalNetPrice = $totalNetPrice;
    $this->modified[self::TOTAL_NET_PRICE] = true;
  }

  public function getTaxRate(): string
  {
    return $this->taxRate;
  }

  public function setTaxRate(string $taxRate): void
  {
    $this->taxRate = $taxRate;
    $this->modified[self::TAX_RATE] = true;
  }

  public function getTaxValue(): string
  {
    return $this->taxValue;
  }

  public function setTaxValue(string $taxValue): void
  {
    $this->taxValue = $taxValue;
    $this->modified[self::TAX_VALUE] = true;
  }

  public function getTotalTaxValue(): string
  {
    return $this->totalTaxValue;
  }

  public function setTotalTaxValue(string $totalTaxValue): void
  {
    $this->totalTaxValue = $totalTaxValue;
    $this->modified[self::TOTAL_TAX_VALUE] = true;
  }

  public function getDiscountValue(): string
  {
    return $this->discountValue;
  }

  public function setDiscountValue(string $discountValue): void
  {
    $this->discountValue = $discountValue;
    $this->modified[self::DISCOUNT_VALUE] = true;
  }

  public function getTotalDiscount(): string
  {
    return $this->totalDiscount;
  }

  public function setTotalDiscount(string $totalDiscount): void
  {
    $this->totalDiscount = $totalDiscount;
    $this->modified[self::TOTAL_DISCOUNT] = true;
  }

  public function getDisplayName(): string
  {
    return $this->displayName;
  }

  public function setDisplayName(string $displayName): void
  {
    $this->displayName = $displayName;
    $this->modified[self::DISPLAY_NAME] = true;
  }

  /**
   * @return \Vijoni\Sales\Shared\ProductChild[]
   */
  public function getChildren(): array
  {
    return $this->children;
  }

  public function setChildren(array $children): void
  {
    $this->children = $children;
    $this->modified[self::CHILDREN] = true;
  }

  /**
   * @return \Vijoni\Sales\Shared\Product[]
   */
  public function getChildProducts(): array
  {
    return $this->childProducts;
  }

  public function setChildProducts(array $childProducts): void
  {
    $this->childProducts = $childProducts;
    $this->modified[self::CHILD_PRODUCTS] = true;
  }

  public function getCount(): int
  {
    return $this->count;
  }

  public function setCount(int $count): void
  {
    $this->count = $count;
    $this->modified[self::COUNT] = true;
  }

  public function getOrderedCount(): int
  {
    return $this->orderedCount;
  }

  public function setOrderedCount(int $orderedCount): void
  {
    $this->orderedCount = $orderedCount;
    $this->modified[self::ORDERED_COUNT] = true;
  }

  public function toArray(): array
  {
    return [
      self::PRODUCT_ID => $this->getProductId(),
      self::PARENT_PRODUCT_ID => $this->getParentProductId(),
      self::IS_ACTIVE => $this->getIsActive(),
      self::NAME => $this->getName(),
      self::TYPE => $this->getType(),
      self::BASE_PRICE => $this->getBasePrice(),
      self::PRICE_TO_PAY => $this->getPriceToPay(),
      self::TOTAL_PRICE_TO_PAY => $this->getTotalPriceToPay(),
      self::NET_PRICE => $this->getNetPrice(),
      self::TOTAL_NET_PRICE => $this->getTotalNetPrice(),
      self::TAX_RATE => $this->getTaxRate(),
      self::TAX_VALUE => $this->getTaxValue(),
      self::TOTAL_TAX_VALUE => $this->getTotalTaxValue(),
      self::DISCOUNT_VALUE => $this->getDiscountValue(),
      self::TOTAL_DISCOUNT => $this->getTotalDiscount(),
      self::DISPLAY_NAME => $this->getDisplayName(),
      self::CHILDREN => $this->getChildren(),
      self::CHILD_PRODUCTS => $this->getChildProducts(),
      self::COUNT => $this->getCount(),
      self::ORDERED_COUNT => $this->getOrderedCount(),
    ];
  }

  public function toDeepArray(): array
  {
    $asArray = $this->toArray();
    $asArray[self::CHILDREN] = $this->collectionToArray($this->getChildren());
    $asArray[self::CHILD_PRODUCTS] = $this->collectionToArray($this->getChildProducts());

    return $asArray;
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::PRODUCT_ID]) && $this->setProductId($properties[self::PRODUCT_ID]);
    isset($properties[self::PARENT_PRODUCT_ID]) && $this->setParentProductId($properties[self::PARENT_PRODUCT_ID]);
    isset($properties[self::IS_ACTIVE]) && $this->setIsActive($properties[self::IS_ACTIVE]);
    isset($properties[self::NAME]) && $this->setName($properties[self::NAME]);
    isset($properties[self::TYPE]) && $this->setType($properties[self::TYPE]);
    isset($properties[self::BASE_PRICE]) && $this->setBasePrice($properties[self::BASE_PRICE]);
    isset($properties[self::PRICE_TO_PAY]) && $this->setPriceToPay($properties[self::PRICE_TO_PAY]);
    isset($properties[self::TOTAL_PRICE_TO_PAY]) && $this->setTotalPriceToPay($properties[self::TOTAL_PRICE_TO_PAY]);
    isset($properties[self::NET_PRICE]) && $this->setNetPrice($properties[self::NET_PRICE]);
    isset($properties[self::TOTAL_NET_PRICE]) && $this->setTotalNetPrice($properties[self::TOTAL_NET_PRICE]);
    isset($properties[self::TAX_RATE]) && $this->setTaxRate($properties[self::TAX_RATE]);
    isset($properties[self::TAX_VALUE]) && $this->setTaxValue($properties[self::TAX_VALUE]);
    isset($properties[self::TOTAL_TAX_VALUE]) && $this->setTotalTaxValue($properties[self::TOTAL_TAX_VALUE]);
    isset($properties[self::DISCOUNT_VALUE]) && $this->setDiscountValue($properties[self::DISCOUNT_VALUE]);
    isset($properties[self::TOTAL_DISCOUNT]) && $this->setTotalDiscount($properties[self::TOTAL_DISCOUNT]);
    isset($properties[self::DISPLAY_NAME]) && $this->setDisplayName($properties[self::DISPLAY_NAME]);
    isset($properties[self::CHILDREN]) && $this->setChildren($properties[self::CHILDREN]);
    isset($properties[self::CHILD_PRODUCTS]) && $this->setChildProducts($properties[self::CHILD_PRODUCTS]);
    isset($properties[self::COUNT]) && $this->setCount($properties[self::COUNT]);
    isset($properties[self::ORDERED_COUNT]) && $this->setOrderedCount($properties[self::ORDERED_COUNT]);
  }

  public function fieldKeys(): array
  {
    return [
      self::PRODUCT_ID => true,
      self::PARENT_PRODUCT_ID => true,
      self::IS_ACTIVE => true,
      self::NAME => true,
      self::TYPE => true,
      self::BASE_PRICE => true,
      self::PRICE_TO_PAY => true,
      self::TOTAL_PRICE_TO_PAY => true,
      self::NET_PRICE => true,
      self::TOTAL_NET_PRICE => true,
      self::TAX_RATE => true,
      self::TAX_VALUE => true,
      self::TOTAL_TAX_VALUE => true,
      self::DISCOUNT_VALUE => true,
      self::TOTAL_DISCOUNT => true,
      self::DISPLAY_NAME => true,
      self::CHILDREN => true,
      self::CHILD_PRODUCTS => true,
      self::COUNT => true,
      self::ORDERED_COUNT => true,
    ];
  }
}
