<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Request;

use Vijoni\Application\SchemaValidator\JsonSchemaValidator;
use Vijoni\Operations\Shared\ValidationMessagesMap;
use Vijoni\Operations\Shared\Customer;

class UpdateCustomerRequest
{
  private const SCHEMA_FILEPATH = __DIR__ . '/schema-update-customer.json';

  private string $payload = '';

  public function __construct(
    private JsonSchemaValidator $jsonSchemaValidator,
    private ValidationMessagesMap $messagesMap
  ) {
  }

  public function use(string $payload): void
  {
    $this->payload = $payload;
  }

  public function validate(): array
  {
    return $this->jsonSchemaValidator->validate(
      $this->payload,
      self::SCHEMA_FILEPATH,
      $this->messagesMap->readMap()
    );
  }

  public function createCustomer(): Customer
  {
    $payload = (array)json_decode($this->payload, true);
    $data = (array)$payload['data'];

    $customer = new Customer();
    $customer->intersect([
      'id' => Customer::CUSTOMER_ID,
      'firstname' => Customer::FIRSTNAME,
      'lastname' => Customer::LASTNAME,
      'email' => Customer::EMAIL,
    ], $data);

    if (isset($data['birthDate'])) {
      /** @var string $birthDate */
      $birthDate = $data['birthDate'];
      $customer->setBirthDate(new \DateTime($birthDate));
    }

    return $customer;
  }
}
