<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedProduct;

class Product extends GeneratedProduct
{
  public function clone(): static
  {
    $clone = clone $this;

    if ($this->hasChildProducts()) {
      $this->cloneChildrenFrom($this, $clone);
    }

    return $clone;
  }

  public function isTypeSet(): bool
  {
    return $this->getType() === self::TYPE_SET;
  }

  public function addChild(ProductChild $productChild): void
  {
    $children = $this->getChildren();
    $children[] = $productChild;

    $this->setChildren($children);
  }

  public function addChildProduct(Product $product): void
  {
    $childProducts = $this->getChildProducts();
    $childProducts[] = $product;

    $this->setChildProducts($childProducts);
  }

  public function hasChildren(): bool
  {
    return !empty($this->getChildren());
  }

  public function hasChildProducts(): bool
  {
    return !empty($this->getChildProducts());
  }

  private function cloneChildrenFrom(Product $source, Product $target): void
  {
    $target->setChildren([]);
    foreach ($source->getChildren() as $child) {
      $target->addChild($child->clone());
    }

    $target->setChildProducts([]);
    foreach ($source->getChildProducts() as $childProduct) {
      $target->addChildProduct($childProduct->clone());
    }
  }
}
